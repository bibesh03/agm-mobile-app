import { Platform, StyleSheet } from 'react-native';

export default StyleSheet.create({

    darkBg: {
        backgroundColor: '#123d65'
    },  
    
    greenColor: {
        // color: '#355F4C'
        color: '#42765E'
    },
    lightBlueColor: {
        color: '#057AAD'
    },
    greenBg: {
        // backgroundColor: '#355F4C'
        backgroundColor: '#42765E'
    },

    lightGreenColor: {
        color: '#00AF54'
    },

    lightGreenBg: {
        backgroundColor: '#00AF54'
    },

    orangeColor: {
        color: '#FF7700'
    },

    orangeBg: {
        backgroundColor: '#FF7700'
    },

    blueColor: {
        color: '#007BFF'
    },

    blueBg: {
        backgroundColor: '#007BFF'
    },

    redColor: {
        color: '#F03F3F'
    },

    redBg: {
        backgroundColor: '#F03F3F'
    },

    darkGreyColor: {
        color: '#546062'
    },

    darkGreyBg: {
        backgroundColor: '#546062'
    },

    greyColor: {
        color: '#A8AAAC'
    },

    greyBg: {
        backgroundColor: '#A8AAAC'
    },

    midGreyColor: {
        color: '#C0C3C4'
    },

    midGreyBg: {
        backgroundColor: '#C0C3C4'
    },

    lightGreyColor: {
        color: '#E4E5E6'
    },

    lightGreyBg: {
        backgroundColor: '#E4E5E6'   
    },

    lighterColor: {
        color: '#F0F0F0'
    },

    lighterBg: {
        // backgroundColor: '#F0F0F0'
        backgroundColor: '#f2f2f2'
    },

    lightestColor: {
        color: '#F7F9FC'
    },

    lightestBg: {
        backgroundColor: '#F7F9FC'
    },

    whiteColor: {
        color: '#FFFFFF'
    },

    whiteBg: {
        backgroundColor: '#FFFFFF'
    },

    blackColor: {
        color: '#000000'
    },

    blackBg: {
        backgroundColor: '#000000'
    },



});