import { Platform, StyleSheet } from 'react-native';
import Color from './colors';

export default StyleSheet.create({

    // BOTTOM SHEET BUTTONS
    baseBottomSheetBtn: {
        justifyContent: 'center', 
        alignItems: 'center', 
        marginTop: 15, 
        paddingVertical: 15, 
        paddingHorizontal: 15, 
        borderRadius: 10
    },

    baseBottomSheetBtnText: {
        fontSize: 18, 
        fontWeight: '500'
    }
})