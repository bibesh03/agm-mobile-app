import { Platfrom, StyleSheet } from 'react-native';
import Color from '../../assets/styles/colors';

export default StyleSheet.create({

    // BASE
    base: {
        backgroundColor: Color.whiteBg.backgroundColor,
        paddingHorizontal: 15, 
        paddingVertical: 20, 
        marginBottom: 15,
        borderRadius: 5,
    },

    // SMALL CARD
    small: {
        flexDirection: 'row',
        // height: 80,
        minHeight: 80,
    },

    // MEDIUM CARD
    medium: {
        flexDirection: 'row',
        // height: 120
        minHeight: 120

    },
    
    // BANNER CARD
    banner: {
        minHeight: 220
    },

    bannerNumBlock: {
        width: '30%', 
        justifyContent: 'flex-start'
    },

    bannerDescriptionBlock: {
        width: '70%', 
        justifyContent: 'flex-start', 
        alignItems: 'flex-end'
    },

    // CONTENT CONTAINER
    contentContainer: {
        width: '90%'
    },  

    // ICON CONTAINER
    iconTopRightContainer: {
        width: '10%',
        alignItems: 'flex-end',
    },
    
    // ICON
    iconClose: {
        marginTop: -5, 
        marginRight: -5,
    }

});