import { Platform, StyleSheet } from 'react-native';
import Color from '../../assets/styles/colors';

export default StyleSheet.create({

    // LAYOUT
    containerPadding: {
        paddingLeft: 15,
        paddingRight: 15
    },

    containerPaddingMed: {
        paddingLeft: 20,
        paddingRight: 20
    },

    containerPaddingMax: {
        paddingLeft: 30,
        paddingRight: 30
    },

    flexContainer: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center'
    },

    mainContainer: {
        flex: 1,
        // width: '100%'
        height: '100%',
    },

    textCenter: {
        textAlign: 'center'
    },

    // ICONS
    iconHeader: {
        width: Platform.OS === 'ios' ? 35 : 100,
        height: Platform.OS === 'ios' ? 35 : 100,
        color: Color.blackColor.color,
        justifyContent: 'center'
    },

    // LOADER
    loaderContainer: {
        flex: 1,
        height: 600,
        justifyContent: 'center',
        alignItems: 'center',
    },
    smallLoaderContainer: {
        flex: 1,
        height: 80,
        justifyContent: 'center',
        alignItems: 'center',
    },

    // TOAST
    toastContainer: {
        zIndex: 1,
        position: 'absolute',
        width: '100%',
        top: 45,
        // top: 700,
        alignItems: 'center',
        shadowColor: Color.darkGreyColor.color,
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.20,
        shadowRadius: 10,

        elevation: 7,
    },

    toast: {
        flexDirection: 'row',
        width: '93%',
        borderRadius: 3,
        borderLeftWidth: 5,
        paddingLeft: 15,
        paddingRight: 7,
        paddingVertical: 10
    },

    // FORM
    formInput: {
        marginBottom: 20
    },

    roundButton: {
        width: '50%',
        alignSelf: 'flex-end',
        borderRadius: 100
    },

    //VALIDATION ERROR MESSAGE
    errorMessageStyle: {
        fontSize: 14,
        color: 'red',
        marginTop: -15,
        marginBottom: 20
    },

    // TASK BLOCK
    taskBlock: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        marginBottom: 15
    },

    taskTitle: {
        fontSize: 14,
    },

    taskStatus: {
        fontSize: 12,
        fontSize: 12,
        fontFamily: 'OpenSans-SemiBold',
    },

    taskIconRight: {
        marginRight: -15,
    },

    // NOTIFICATIONS
    notyIndicator: {
        paddingHorizontal: 10,
        paddingVertical: 5,
        marginBottom: 15,
        borderRadius: 5,
        alignSelf: 'flex-start'
    },

    // LIST BASE
    listBase: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 15,
        borderBottomWidth: 1,
        borderBottomColor: Color.lightGreyColor.color
    },

    listBaseHeaderLarge: {
        fontSize: 25,
        fontWeight: '700',
        color: Color.greenColor.color,
        opacity: 0.9
    },

    listBaseHeaderSmall: {
        fontSize: 14,
        fontWeight: '700',
        color: Color.greenColor.color,
        textTransform: 'uppercase',
        opacity: 0.5
    },

    listBaseText: {
        fontSize: 16,
        fontWeight: '500'
    },

    // BLOCK COMPONENTS
    titleBlock: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },

    titleBlockBorder: {
        borderBottomWidth: 1,
        borderBottomColor: Color.lightGreyColor.color
    },

    titleBlockContainer: {
        width: '80%',
    },

    titleBlockText: {
        fontWeight: '700',
        fontSize: 18,
        color: Color.greenColor.color,
        paddingBottom: 15
    },

    // LIST BLOCK
    listBlock: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingVertical: 13,
        borderBottomWidth: 1,
        borderBottomColor: Color.lightGreyColor.color
    },

    listBlockTitleContainer: {
        width: '70%',
        flexDirection: 'row',
        alignItems: 'center',
    },

    listBlockTitle: {
        fontWeight: '600',
        fontFamily: 'OpenSans-SemiBold',
        fontSize: 16,
        textTransform: 'capitalize',
        // color: Color.greenColor.color, 
        color: Color.blackColor.color,
        opacity: 0.8
    },

    listBlockTitleAlt: {
        fontSize: 12,
        fontFamily: 'OpenSans-Bold',
        textTransform: 'uppercase'
    },

    listBlockSubtitle: {
        fontFamily: 'OpenSans-Regular',
        fontSize: 14,
        color: Color.greyColor.color,
        marginTop: 3,
        opacity: 0.8
    },

    listBlockSubtitleAlt: {
        width: '100%',
        fontWeight: '500',
        fontSize: 18,
        color: Color.blackColor.color,
        marginTop: 7,
        opacity: 0.8,
    },

    listBlockBadge: {
        paddingVertical: 5,
        paddingHorizontal: 10,
        borderRadius: 30,
        marginRight: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },

    listBlockBadgeText: {
        fontSize: 12,
        fontWeight: '600',
        color: Color.whiteColor.color
    },

    listBlockIconRightContainer: {
        width: 35,
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 30
    },

    listBlockFooter: {
        borderBottomWidth: 0,
        paddingTop: 15,
        paddingBottom: 0
    },

    listBlockFooterText: {
        fontWeight: '500',
        fontSize: 16,
        color: Color.blueColor.color
    },

    // ACTION BLOCK
    actionBlock: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingVertical: 15
    },

    actionBlockTitleContainer: {
        width: '80%',
    },

    actionBlockTitle: {
        fontWeight: '700',
        fontFamily: 'OpenSans-SemiBold',
        fontSize: 18,
        color: Color.blackColor.color,
        opacity: 0.7
    },

    // FIXED BUTTON
    fixedButtonContainer: {
        backgroundColor: Color.greenBg.backgroundColor,
        height: 55,
        width: 55,
        borderRadius: 100,
        justifyContent: 'center',
        position: "absolute",
        // top: '77%', 
        top: Platform.OS === 'ios' ? 560 : 590,
        // left: '80%',
        left: Platform.OS === 'ios' ? 300 : 320,
        zIndex: 1,

        //BOX SHADOW
        shadowColor: Color.blackColor.color,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.2,
        shadowRadius: 5,

        elevation: 5,
    },

    // FILE UPLOAD
    imageContainer: {
        height: 300,
        // marginTop: 0,
        marginBottom: 30,
        justifyContent: 'center',
        alignItems: 'center',
    },

    imageWrap: {
        width: '100%',
        height: '100%',
        // marginBottom: 40,
        paddingTop: 20,
        paddingBottom: 30,
        // backgroundColor: Color.lightGreyBg.backgroundColor,
        borderRadius: 5
    },

    image: {
        flex: 1,
        width: null,
        height: null,
        resizeMode: 'contain',
    },
    // BOTTOM SHEET
    bottomSheetRadius: {
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10
    },

    bottomSheetTitleBorder: {
        borderBottomWidth: 1,
        borderBottomColor: Color.lightGreyColor.color
    },

    bottomSheetTitleElementContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingBottom: 10
    },

    bottomSheetStaticTitleText: {
        color: Color.darkGreyColor.color,
        fontWeight: '500',
        fontSize: 16
    },

    bottomSheetTouchableTitleText: {
        color: Color.lightBlueColor.color,
        fontWeight: '600',
        fontSize: 16,
    },

    // BOTTOM SHEET STATIC HEADER
    bottomSheetStaticHeaderContainer: {
        width: '100%',
        alignItems: 'center',
        borderBottomColor: Color.lightGreyColor.color,
        borderBottomWidth: 1,
        paddingTop: 20,
        paddingBottom: '5%'
    },


    bottomSheetStaticHeaderText: {
        fontSize: 16,
        opacity: 0.8,
        textAlign: 'center'
    },

    // --INPUT
    inputMainContainer: {
        paddingHorizontal: 0
    },

    inputContainer: {
        borderBottomWidth: 1,
        borderBottomColor: Color.midGreyColor.color,
        marginTop: 0,
        paddingBottom: 5
    },

    inputStyle: {
        color: Color.greenColor.color,
        opacity: 0.8
    },

    numericInput: {
        fontSize: 20,
        fontWeight: '500',
        letterSpacing: 0.5
    },

    // --INPUT BORDERED

    inputBorderedMainContainer: {
        paddingHorizontal: 0
    },

    inputBorderedContainer: {
        borderWidth: 1,
        borderColor: "#e9edf5",
        backgroundColor: "#f7f8fc",
        // backgroundColor: Color.whiteBg.backgroundColor,
        // borderRadius: 4,
        borderRadius: 100,
        paddingHorizontal: 15,
        // paddingRight: 10,
        width: "100%",
        minHeight: 20,
    },

    inputBorderedStyle: {
        fontSize: 16,
        color: Color.blackColor.color,
        opacity: 0.8,
    },

    // MESSAGE PLATFORM

    mainMessageBlockContainer: {
        width: '100%',
        flexDirection: 'row',
        marginBottom: 15,
    },

    messageContainer: {
        maxWidth: '85%',
        minWidth: '50%',
        minHeight: 50,
        padding: 15,
        borderRadius: 10,
    },

    // leftMessageBlockContainer: {
    // },

    // rightMessageBlockContainer: {

    // },

    messageBlockContainer: {
        alignItems: 'flex-start',
        marginBottom: 10,
    },

    messageText: {
        fontSize: 16,
    },

    timeStampContainer: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
    },

    messageTimeStamp: {
        fontSize: 12,
        marginRight: 3
    },

    messageMainInputContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 15,
        paddingVertical: 10,
        paddingTop: 20
    },

    messageInputContainer: {
        width: '84%'
    },

    messageSendButtonContainer: {
        width: '15%',
        alignItems: 'flex-end'
    },

    messageSendIcon: {
        width: 40,
        height: 40,
        borderRadius: 100,
        justifyContent: 'center',
        alignItems: 'center',
    },

    // SWIPE ROW
    swipeListActionContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        paddingRight: 18,
    },

    swipeListActionText: {
        fontSize: 14,
    },

    // MODAL HEADER
    modalHeaderLeftElementContainer: {
        width: '20%',
        alignContent: 'flex-start'
    },

    modalHeaderCenterElementContainer: {
        width: '60%',
        alignItems: 'center'
    },

    modalHeaderRightElementContainer: {
        alignItems: 'flex-end',
        width: '20%'
    },

    modalHeaderTitle: {
        fontSize: 20,
        fontWeight: '600',
        fontFamily: 'OpenSans-SemiBold',
        color: Color.blackColor.color
    },

    modalHeaderDismissIconContainer: {
        backgroundColor: Color.lightGreyBg.backgroundColor,
        borderRadius: 100
    },

    modalHeaderDismissIcon: {
        color: Color.darkGreyColor.color,
        padding: 4,
        opacity: 0.6
    },


});