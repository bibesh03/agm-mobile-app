import { Platform, StyleSheet } from 'react-native';
import Color from '../styles/colors';

export default StyleSheet.create({

    // HEADING
    h1: {
        fontSize: 50,
        fontFamily: 'OpenSans-Bold',
        color: Color.greenColor.color,
    },

    // PARAGRAPHS
    p2: {
        fontSize: 16,
        fontFamily: 'OpenSans-Regular',
    },

    // SUBTITLES
    s1: {
        fontSize: 20,
        fontFamily: 'OpenSans-Regular',
        color: Color.darkGreyColor.color,
        paddingBottom: 10,
    },


    // LABELS (formLabel)
    formLabel: {
        fontSize: 16,
        fontFamily: 'OpenSans-SemiBold',
        color: Color.greenColor.color,
        paddingBottom: 10,
        opacity: 0.8
    },

    // FORM INPUT
    formInput: {
        fontSize: 18,
        fontFamily: 'OpenSans-Regular',
        color: Color.greenColor.color,
    },

    formInputMultiLine: {
        fontSize: 18,
        fontFamily: 'OpenSans-Regular',
        color: Color.greenColor.color,
        // minHeight:100
        paddingBottom:70
    },

    // FORM BUTTON
    formButton: {
        // fontSize: 22,
        fontSize: 11,
        fontFamily: 'OpenSans-Bold',
        color: Color.whiteColor.color,
        letterSpacing: 0.5,
    },

    // CARDS

    // BANNER
    cardBannerNumber: {
        fontSize: 80, 
        fontFamily: 'OpenSans-Bold',
        color: Color.blackColor.color, 
        marginTop: -25
    },

    cardBannerSubtitle: {
        fontSize: 27, 
        fontFamily: 'OpenSans-SemiBold',
        color: Color.blackColor.color, 
        marginTop: -5
    },

    cardBannerDescription: {
        fontSize: 16, 
        fontFamily: 'OpenSans-Bold',
        color: Color.greenColor.color
    },

    // MEDIUM
    cardMediumTitle: {
        fontSize: 22,
        textTransform: 'capitalize',
        color: Color.blackColor.color, 
        paddingBottom: 5
    },

    cardMediumDescription: {
        fontSize: 14, 
        fontFamily: 'OpenSans-Bold',
        color: Color.greenColor.color
    },

    // SMALL
    cardSmallTitle: {
        fontSize: 14,
        color: Color.blackColor.color, 
    },

    cardSmallDescription: {
        fontSize: 14, 
        fontFamily: 'OpenSans-Bold',
        color: Color.greenColor.color
    },

    // CARD BUTTON
    cardButton: {
        // fontSize: 16,
        fontSize: 16,
        fontFamily: 'OpenSans-SemiBold',
        color: Color.whiteColor.color,

    },

    // MESSAGE CENTER
    notySubtitle: {
        fontSize: 16,
        color: Color.whiteColor.color
    }


});