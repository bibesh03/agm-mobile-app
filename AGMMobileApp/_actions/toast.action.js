import ToastConstants from '../_constants/toast.constants';


export const ShowToast = (model) => ({
    type: ToastConstants.SHOW_TOAST,
    model: model
})


export const HideToast = () => ({
    type: ToastConstants.HIDE_TOAST
})