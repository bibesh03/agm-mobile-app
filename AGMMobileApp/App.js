import React, { useState, useEffect, useRef } from 'react';
import { SafeAreaView, StyleSheet, YellowBox } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer, useNavigation } from '@react-navigation/native';
import { AppLoading } from 'expo';
import * as Font from 'expo-font';
import AuthStackScreen from './components/NavStack/StackNavigation/AuthStackScreen';
import * as eva from '@eva-design/eva';
import { ApplicationProvider, IconRegistry } from '@ui-kitten/components';
import { EvaIconsPack } from '@ui-kitten/eva-icons';
import { default as theme } from './custom-theme.json';
import { default as mapping } from './mapping.json';
import Toast from './components/shared/Toast';
import * as Notifications from 'expo-notifications';
import * as Permissions from 'expo-permissions';
import Constants from 'expo-constants';
import { storePushNotificationToken } from './_services/authenticateService';

const RootStack = createStackNavigator();

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: false,
    shouldSetBadge: false,
  }),
});

const App = () => {
  const [load, setLoad] = useState(false);
  const [expoPushToken, setExpoPushToken] = useState('');
  const [notification, setNotification] = useState(false);
  const notificationListener = useRef();
  const responseListener = useRef();

  useEffect(() => {
    const loadFonts = async () => {

      await Font.loadAsync({
        'OpenSans-Light': require("./assets/fonts/OpenSans-Light.ttf"),
        'OpenSans-LightItalic': require("./assets/fonts/OpenSans-LightItalic.ttf"),
        'OpenSans-Regular': require("./assets/fonts/OpenSans-Regular.ttf"),
        'OpenSans-Italic': require("./assets/fonts/OpenSans-Italic.ttf"),
        'OpenSans-SemiBold': require("./assets/fonts/OpenSans-SemiBold.ttf"),
        'OpenSans-SemiBoldItalic': require("./assets/fonts/OpenSans-SemiBoldItalic.ttf"),
        'OpenSans-Bold': require("./assets/fonts/OpenSans-Bold.ttf"),
        'OpenSans-BoldItalic': require("./assets/fonts/OpenSans-BoldItalic.ttf"),
        'OpenSans-ExtraBold': require("./assets/fonts/OpenSans-ExtraBold.ttf"),
        'OpenSans-ExtraBoldItalic': require("./assets/fonts/OpenSans-ExtraBoldItalic.ttf"),
      });
        setLoad(true);
    };

    loadFonts();
    RegisterPushNotificationAsync().then(token => {
        setExpoPushToken(token);
    });
    // This listener is fired whenever a notification is received while the app is foregrounded
    notificationListener.current = Notifications.addNotificationReceivedListener(notification => {
        setNotification(notification);
    });

    // This listener is fired whenever a user taps on or interacts with a notification (works when app is foregrounded, backgrounded, or killed)
    responseListener.current = Notifications.addNotificationResponseReceivedListener(response => {
    });

    return () => {
      Notifications.removeNotificationSubscription(notificationListener);
      Notifications.removeNotificationSubscription(responseListener);
    };
  }, []);

  const RegisterPushNotificationAsync = async () => {
    let token;
    if (Constants.isDevice) {
      const { status: existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
      let finalStatus = existingStatus;
      if (existingStatus !== 'granted') {
        const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
        finalStatus = status;
      }
      // if (finalStatus !== 'granted') {
      //   alert('Failed to get push token for push notification!');
      //   return;
      // }
      token = (await Notifications.getExpoPushTokenAsync()).data;
      storePushNotificationToken(token).then(res => {
      }).catch(err => console.log('something went wrong here'));
    } else {
      alert('Must use physical device for Push Notifications');
    }

    if (Platform.OS === 'android') {
      Notifications.setNotificationChannelAsync('default', {
        name: 'default',
        importance: Notifications.AndroidImportance.MAX,
        vibrationPattern: [0, 250, 250, 250],
        lightColor: '#FF231F7C',
      });
    }

    return token;
  }

  if (!load) {
    return <AppLoading />;
  }
  return (
    <>
      <IconRegistry icons={EvaIconsPack} />
      <ApplicationProvider
        {...eva}
        theme={{ ...eva.light, ...theme }}
        customMapping={mapping}
      >
        <Toast />
        <NavigationContainer>
          <RootStack.Navigator
            screenOptions={({ navigation }) => ({
              headerShown: false,
              headerTitleAlign: 'center',
            })}
          >
            <RootStack.Screen name="AuthStackScreen" component={AuthStackScreen} />
          </RootStack.Navigator>
        </NavigationContainer>
      </ApplicationProvider>
    </>
  )
}
export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

