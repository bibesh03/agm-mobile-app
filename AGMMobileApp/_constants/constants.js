const constants = {
    API_URL: 'http://agmapi.primtek.net/',
    
    PORTAL_URL: 'http://agmportal.primtek.net/'

    // API_URL: 'http://a15df8c83828.ngrok.io/',
    
    // PORTAL_URL: 'http://localhost:51000/'
};

export default constants;
