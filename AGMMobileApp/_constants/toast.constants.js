const ToastConstants = {
    HIDE_TOAST: 'HIDE_TOAST',
    SHOW_TOAST: 'SHOW_TOAST',
}

export default ToastConstants;