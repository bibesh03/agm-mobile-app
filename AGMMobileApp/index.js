import { registerRootComponent } from 'expo';
import React from 'react';
import App from './App';
import rootReducer from './_reducers/reducer';
import { createStore } from 'redux';
import { Provider } from 'react-redux';

const rootStore = createStore(rootReducer);

// registerRootComponent calls AppRegistry.registerComponent('main', () => App);
// It also ensures that whether you load the app in the Expo client or in a native build,
// the environment is set up appropriately

const mainComp = (props) => {
    return (
        <Provider store={rootStore}>
            <App/>
        </Provider>
    )
}

registerRootComponent(mainComp);
