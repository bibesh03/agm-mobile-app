import * as Yup from "yup";

export default Yup.object().shape({

    username: Yup.string()
        .required("Please enter your username"),

    password: Yup.string()
        .required('Please enter your password')
});
