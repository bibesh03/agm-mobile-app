import * as Yup from "yup";

export default Yup.object().shape({
    FirstName: Yup.string()
        .required("Please enter your first name"),

    LastName: Yup.string()
        .required("Please enter your last name"),

    EmailAddress: Yup.string()
        .required("Please enter your email address")
        .email("Please enter a valid email address"),

    Client_AccessCode: Yup.string()
        .required("Please enter your client access code"),

    HireDate: Yup.string()
        .required("Please enter your hire date"),

    Username: Yup.string()
        .required("Please enter a username"),

    SSN: Yup.string()
        .required("Please enter your SSN")
        .matches('^(?!666|000|9\\d{2})\\d{3}-(?!00)\\d{2}-(?!0{4})\\d{4}$',
            'Enter a Valid SSN',
        ),

    DateOfBirth: Yup.string()
        .required("Please enter your date of birth"),
});
