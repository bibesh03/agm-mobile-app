import * as Yup from "yup";

export default Yup.object().shape({

    CurrentPassword: Yup.string()
        .required("Please enter your current password"),

        NewPassword: Yup.string()
        .required("Please enter your new password"),

        ConfirmPassword: Yup.string()
        .required("Please confirm your password")
        .oneOf([Yup.ref('NewPassword')], 'New Password and Confirm password must match')
});
