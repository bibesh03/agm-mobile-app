import _ from 'lodash';
import React, { useState, useEffect } from 'react';
import { Spinner } from '@ui-kitten/components';
import { View } from 'react-native';
import Style from '../assets/styles/styles';

const LoadingSmallContainer = () => {
        return (
                <View style={[Style.smallLoaderContainer]}>
                        <Spinner size='large' />
                </View>
        )
}

export default LoadingSmallContainer;