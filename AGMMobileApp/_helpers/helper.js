import moment from 'moment';
import _ from 'lodash';

const GroupByMonth = (doc) => _.groupBy(doc, (doc) => moment(doc.UpdatedOn).format('MMMM'));
const GroupByFolder = (doc) => _.groupBy(doc, (doc)=> doc.FolderName);
const GroupByProviders = (provider) => _.groupBy(provider, (provider)=> provider.ProviderType);
const GroupByIsChecked = (task) => _.groupBy(task, (task)=> task.IsChecked);
const SortByTimeDescending = (res) => _.sortBy(res, function(o) { 
                                                return new moment(o.CreatedOn);
                                                }).reverse()
const Capitalize = (words) =>
{
    return words && words[0].toUpperCase() + words.slice(1);
}
export {
    GroupByMonth,
    GroupByFolder,
    GroupByIsChecked,
    SortByTimeDescending,
    GroupByProviders,
    Capitalize
}