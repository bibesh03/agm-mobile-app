import axiosInstance from './apiService';
import { GetLoggedInUserId } from './authenticateService';

const sendSupportMessage = async (supportRequest) => await axiosInstance.post('PostMessage', supportRequest, {
    requestHandlerEnabled: false,
    responseHandlerEnabled: true
});
const postIsRead = async (supportRequest) => await axiosInstance.post('PostIsRead', supportRequest, {
    requestHandlerEnabled: false,
    responseHandlerEnabled: true
});

const getNotification = async () => await axiosInstance.get(`GetNotifications?UserId=${await GetLoggedInUserId()}`, {
    requestHandlerEnabled: false,
    responseHandlerEnabled: true
});

const getSupportMessage = async () => await axiosInstance.get(`GetMessages?userId=${await GetLoggedInUserId()}`, {
    requestHandlerEnabled:false,
    responseHandlerEnabled:true
});

const getConversationMessage = async (threadId, count) => await axiosInstance.get(`GetConversation?userId=${await GetLoggedInUserId()}&threadId=${threadId}&count=${count}`, {
    requestHandlerEnabled:false,
    responseHandlerEnabled:true
});

const getIsNotReadCount = async () => await axiosInstance.get(`GetIsNotReadCount?UserId=${await GetLoggedInUserId()}&threadId=${threadId}`, {
    requestHandlerEnabled: false,
    responseHandlerEnabled: true
});


const postMessageAsIsRead = async () => await axiosInstance.get(`MessageIsRead`, {
    requestHandlerEnabled: false,
    responseHandlerEnabled: true
});

export {
    sendSupportMessage,
    getNotification,
    postMessageAsIsRead,
    getSupportMessage,
    getConversationMessage,
    getIsNotReadCount,
    postIsRead
}