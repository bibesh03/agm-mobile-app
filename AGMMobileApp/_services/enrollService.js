import axiosInstance from './apiService';
import { GetClientID} from './authenticateService';

const getGroupUrl = async () => await axiosInstance.get(`GetGroupUrl?clientId=${await GetClientID()}`, {
    requestHandlerEnabled: false,
    responseHandlerEnabled: true
});

export {
    getGroupUrl,
}