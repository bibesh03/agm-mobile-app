import AsyncStorage from "@react-native-community/async-storage";

const isEnabled = (config, property) => config.hasOwnProperty(property) && config[property];

//handles requests going from app to server
const requestHandler = async (request) => {
    if (isEnabled(request, 'requestHandlerEnabled')) {
        let userData = await AsyncStorage.getItem("user");
        let data = JSON.parse(userData);
        request.headers['Authorization'] = `Bearer ${data.token}`;
    }
    request.headers['Content-Type'] = 'application/json';
    request.headers['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS';
    request.headers[ 'Access-Control-Allow-Origin'] = '*';
  
    return request;
}

//handles responses coming from server to app
const successHandler = (response) => {
    if (isEnabled(response.config, 'responseHandlerEnabled')) {
        return response.data;
        
    }
    return response;
}
const errorHandler = (response) => {
    if (isEnabled(response.config, 'responseHandlerEnabled')) {
    }

    return Promise.reject(response.response);
}

export {
    requestHandler,
    successHandler,
    errorHandler
}