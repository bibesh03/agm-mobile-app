import axiosInstance from './apiService';
import { GetLoggedInUserId, GetClientID, getUser } from './authenticateService';

const getUserDocuments = async (isImage) => await axiosInstance.get(`GetUserDocuments?UserId=${await GetLoggedInUserId()}&isImage=${isImage}`, {
    requestHandlerEnabled: true,
    responseHandlerEnabled: true
});

const getBenefitsDocuments = async () => await axiosInstance.get(`GetBenefitsDocuments?UserId=${await GetLoggedInUserId()}&clientId=${await GetClientID()}`, {
    requestHandlerEnabled: true,
    responseHandlerEnabled: true
});

const viewDocuments = async (docId) => await axiosInstance.get(`Download?id=${docId}`, {
    requestHandlerEnabled: true,
    responseHandlerEnabled: true
});

const uploadDocuments = async (userId, docInfoId, genDocId, formData) => await axiosInstance.post(`Upload?UserID=${userId}&&docInfoId=${docInfoId}&&genDocId=${genDocId}`, formData, {
    requestHandlerEnabled: true,
    responseHandlerEnabled: true
});

const replaceDocuments = async (userId, docInfoId, genDocId, formData) => await axiosInstance.post(`Upload?UserID=${userId}&&docInfoId=${docInfoId}&&genDocId=${genDocId}`, formData, {
    requestHandlerEnabled: true,
    responseHandlerEnabled: true
});

const deleteUserDocument = async (genDocId,docInfoId) => await axiosInstance.delete(`DeleteDocument?genDocId=${genDocId}&&docInfoId=${docInfoId}`, {
    requestHandlerEnabled: true,
    responseHandlerEnabled: true
});

const getProviders = async () => await axiosInstance.get(`GetProviders?clientId=${await GetClientID()}`, {
    requestHandlerEnabled: true,
    responseHandlerEnabled: true
});

const getLinks = async () => await axiosInstance.get(`GetLinks?clientId=${await GetClientID()}`, {
    requestHandlerEnabled: true,
    responseHandlerEnabled: true
});


export {
    getUserDocuments,
    replaceDocuments,
    getBenefitsDocuments,
    viewDocuments,
    uploadDocuments,
    getProviders,
    getLinks,
    deleteUserDocument
}