import axios from 'axios';
import constants from '../_constants/constants';
import { requestHandler, successHandler, errorHandler } from './interceptors';

const axiosInstance = axios.create({
    baseURL:  constants.API_URL,
    withCredentials: true,
});

axiosInstance.interceptors.request.use(
    request => requestHandler(request)
);

axiosInstance.interceptors.response.use(
    response => successHandler(response),
    err => errorHandler(err)
);

export default axiosInstance;

