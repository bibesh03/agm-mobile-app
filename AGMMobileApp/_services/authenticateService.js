import axiosInstance from './apiService';
import AsyncStorage from '@react-native-community/async-storage';

const authenticate = async (loginModel) => await axiosInstance.post('Authenticate', loginModel, {
  requestHandlerEnabled: false,
  responseHandlerEnabled: true
});

const register = async (registerModel) => await axiosInstance.post('Register', registerModel, {
  requestHandlerEnabled: false,
  responseHandlerEnabled: true
});

const updateInfo = async (userModel) => await axiosInstance.post('UpdateInfo', userModel, {
  requestHandlerEnabled: false,
  responseHandlerEnabled: true
});

const resetPassword = async (resetPasswordModel) => await axiosInstance.post('ResetPassword', resetPasswordModel, {
  requestHandlerEnabled: false,
  responseHandlerEnabled: true
});

const enroll = async (IsEnrolled) => await axiosInstance.post(`Enroll?UserId=${await GetLoggedInUserId()}&IsEnrolled=${IsEnrolled}`, {
  requestHandlerEnabled: true,
  responseHandlerEnabled: true
});

const updateUserPasswod = async (updatePasswordModel) => await axiosInstance.post('ResetPassword', updatePasswordModel, {
  requestHandlerEnabled: true,
  responseHandlerEnabled: true
});

const storeToken = async (user) => {
  try {
    await AsyncStorage.setItem("user", JSON.stringify(user));
  } catch (error) {
    console.log("Something went wrong", error);
  }
}

const storePushNotificationToken = async (token) => {
  try {
    await AsyncStorage.setItem("pushToken", JSON.stringify(token));
  } catch (error) {
    console.log("Something went wrong on storing the token", error);
  }
}

const getPushNotificationToken = async () => {
  try {
    let tokenData = await AsyncStorage.getItem("pushToken");
    let data = JSON.parse(tokenData);
    return data;
  } catch (error) {
    console.log("Something went wrong", error);
  }
}

const getToken = async () => {
  try {
    let userData = await AsyncStorage.getItem("user");
    let data = JSON.parse(userData);
    return data;
  } catch (error) {
    console.log("Something went wrong", error);
  }
}

const logOut = async () => {
  try {
    await AsyncStorage.removeItem('user');
  } catch (error) {
    console.log("Error resetting data" + error);
  }
}

const logOutUser = async () => await axiosInstance.post('Logout', {
  requestHandlerEnabled: false,
  responseHandlerEnabled: true
});

const GetLoggedInUserId = async () => {
  var loggedInUser = await AsyncStorage.getItem("user");

  return JSON.parse(loggedInUser).UserID;
}
const getUser = async () => {
  var loggedInUser = await AsyncStorage.getItem("user");

  return JSON.parse(loggedInUser);
}

const GetClientID = async () => {
  var loggedInUser = await AsyncStorage.getItem("user");

  return JSON.parse(loggedInUser).ClientID;
}
export {
  authenticate,
  register,
  updateInfo,
  getToken,
  storeToken,
  logOut,
  GetLoggedInUserId,
  logOutUser,
  getPushNotificationToken,
  storePushNotificationToken,
  GetClientID,
  getUser,
  enroll,
  resetPassword,
  updateUserPasswod
}