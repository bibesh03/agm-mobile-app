import axiosInstance from './apiService';
import AsyncStorage from '@react-native-community/async-storage';
import { GetLoggedInUserId } from './authenticateService';

const getTasks = async (IsChecked) => await axiosInstance.get(`GetTaskByCompletion?UserId=${await GetLoggedInUserId()}&IsChecked=${IsChecked}`, {
    requestHandlerEnabled: true,
    responseHandlerEnabled: true
});

const getIndividualTask = async (clientEmployee_CETaskId) => await axiosInstance.get(`GetIndividualTask?clientEmployee_CETaskId=${clientEmployee_CETaskId}`, {
    requestHandlerEnabled: true,
    responseHandlerEnabled: true
});

const getAllTasks = async () => await axiosInstance.get(`GetAllTask?UserId=${await GetLoggedInUserId()}`, {
    requestHandlerEnabled: true,
    responseHandlerEnabled: true
});

const viewTask = async (taskId) => await axiosInstance.get(`Download?id=${taskId}`, {
    requestHandlerEnabled: true,
    responseHandlerEnabled: true
});

const checkTask = async (taskModel) => await axiosInstance.post('CheckComplete', taskModel, {
    requestHandlerEnabled: true,
    responseHandlerEnabled: true
});

export {
    getTasks,  
    getAllTasks,
    viewTask,
    checkTask,
    getIndividualTask
}