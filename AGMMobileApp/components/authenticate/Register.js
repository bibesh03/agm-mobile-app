import React, { useState, useEffect, useRef } from 'react';
import Validator from '../../_helpers/RegisterValidator';
import { Formik } from 'formik';
import { StyleSheet, View, SafeAreaView, Platform, Modal, Keyboard, ScrollView, StatusBar } from 'react-native';
import DatePicker from 'react-native-datepicker';
import DateTimePicker from '@react-native-community/datetimepicker';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Style from '../../assets/styles/styles';
import Color from '../../assets/styles/colors';
import FontStyle from '../../assets/styles/font';
import Button from '../shared/Button';
import { Text, Input, Icon, Spinner } from '@ui-kitten/components';
import { TextInputMask } from 'react-native-masked-text'
import { register } from '../../_services/authenticateService';
import { ShowToast } from '../../_actions/toast.action';
import { connect } from 'react-redux';
import moment from 'moment';
import BottomSheet from "react-native-raw-bottom-sheet";
import { getPushNotificationToken } from '../../_services/authenticateService';
import BottomSheetHeader from '../shared/BottonSheetHeader';

const Register = (props) => {
  const { ShowToast } = props;
  const refBottomSheetDOB = useRef();
  const refBottomSheetHireDate = useRef();
  const [registerValues, setRegisterValues] = useState({ UserId: '', FirstName: '', LastName: '', EmailAddress: '', HireDate: new Date(), SSN: '', Client_AccessCode: '', DateOfBirth: new Date(), Username: '', Password: '', ConfirmPassword: '', pushNotificationToken: '',isEnrolled:false });
  const [showHireDate, setShowHireDate] = useState(false);
  const [pushNotificationValue, setPushNotificationValue] = useState(null);
  const [showDOB, setShowDOB] = useState(false);
  const [modalVisible, setModalVisible] = useState(false)
  const [maskedText, setMaskedText] = useState(null);
  const [secureTextEntryPassword, setSecureTextEntryPassword] = React.useState(true);
  const [secureTextEntryCPassword, setSecureTextEntryCPassword] = React.useState(true);
  const [enableResetScroll, setEnableResetScroll] = useState(true);

  useEffect(() => {
    const getPushNotificationTokenHere = async () => {
        setPushNotificationValue(await getPushNotificationToken());
    }
    getPushNotificationTokenHere();
  }, [])

  const setInputStatus = (touched, hasError) => {
    if (hasError) {
      return 'danger';
    } else if (touched && !hasError) {
      return 'success';
    } else {
      return ''
    }
  }
  const registerUser = async (model, setSubmitting) => {

    model.pushNotificationToken = pushNotificationValue;
    register(model).then(res => {
        props.navigation.navigate('Login');
        ShowToast({ 'message': res.Message, 'type': res.Success ? 'success' : 'warning', "position": "bottom" });
      setSubmitting(false);
    }).catch(err => {
      //handle error
    }
    )
  }
  // USER ICON
  const userIcon = (props) => (
    <Icon
      {...props}
      fill='#8F9BB3'
      name='person-outline' />
  );

  //Show date picker
  const showDatePickerHireDate = () => {
    setShowHireDate(true);
  }

  const showDatePickerDOB = () => {
    setShowDOB(true);
  }

  const onChangeHireDate = (event, selectedDate) => {
    setRegisterValues({ ...registerValues, HireDate: selectedDate });
    setShowHireDate(false);
  }

  const onChangeDOB = (event, selectedDate) => {
    setRegisterValues({ ...registerValues, DateOfBirth: selectedDate });
    setShowDOB(false);
  }

  //SET MIN MAX DATE FOR DATEPICKER

  // EMAIL ICON
  const emailIcon = (props) => (
    <Icon
      {...props}
      fill='#8F9BB3'
      name='at-outline' />
  );

  // CALENDAR ICON FOR DATEOFBIRTH
  const calendarIconDOB = (props) => (
    <Icon
      {...props}
      fill='#8F9BB3'
      name='calendar-outline' />
  );

  // CALENDAR ICON FOR HIRE DATE
  const calendarIconHire = (props) => (
    <Icon
      {...props}
      fill='#8F9BB3'
      name='calendar-outline' />
  );

  // HASH ICON
  const hashIcon = (props) => (
    <Icon
      {...props}
      fill='#8F9BB3'
      name='hash-outline' />
  );

  //MASKING FUNCTION

  const FormatSocialSecurity = (val) => {
    val = val.replace(/\D/g, '');
    val = val.replace(/^(\d{3})/, '$1-');
    val = val.replace(/-(\d{2})/, '-$1-');
    val = val.replace(/(\d)-(\d{4}).*/, '$1-$2');

    return val;
  }

  // PASSWORD INPUT ICON

  const toggleSecureEntryPassword = () => {
    setSecureTextEntryPassword(!secureTextEntryPassword);
  }

  const toggleSecureEntryCPassword = () => {
    setSecureTextEntryCPassword(!secureTextEntryCPassword);
  }
  const showPassword = (props) => (

    <TouchableWithoutFeedback onPress={toggleSecureEntryPassword}>
      <Icon {...props} name={secureTextEntryPassword ? 'eye-off-outline' : 'eye-outline'} />
    </TouchableWithoutFeedback>
  );

  const showConfirmPassword = (props) => (

    <TouchableWithoutFeedback onPress={toggleSecureEntryCPassword}>
      <Icon {...props} name={secureTextEntryCPassword ? 'eye-off-outline' : 'eye-outline'} />
    </TouchableWithoutFeedback>
  );

  const onCloseDOB = () => {
    setShowDOB(false);

  }

  const openBottomSheetDOB = () => {
    refBottomSheetDOB.current.open();
    Keyboard.dismiss();
  }

  const closeBottomSheetDOB = () => {
    refBottomSheetDOB.current.close();
    Keyboard.dismiss();
  }

  const openBottomSheetHireDate = () => {
    refBottomSheetHireDate.current.open();
    // setEnableResetScroll(false);
    Keyboard.dismiss();
  }

  const closeBottomSheetHireDate = () => {
    refBottomSheetHireDate.current.close();
    // setEnableResetScroll(true);
    Keyboard.dismiss();
  }

  // LOADING INDICATOR
  const loadingIndicator = (props) => (
    <View style={[props.style]}>
      <Spinner status='basic' size='small' />
    </View>
  );
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={[Color.whiteBg, Style.mainContainer]}>
        <Formik
          initialValues={registerValues}
          validationSchema={Validator}
          onSubmit={(values, { setSubmitting }) => {
            setRegisterValues(values);
            registerUser(values, setSubmitting);
            setSubmitting(true);
          }}
        >

          {({ handleChange, handleBlur, handleSubmit, values, errors, touched, isValid, setFieldTouched, isSubmitting, setFieldValue }) => (
            <KeyboardAwareScrollView
              extraScrollHeight={0} 
              enableResetScrollToCoords={false}
              enableOnAndroid={true}
              style={[Style.containerPaddingMed, { flex: 1, backgroundColor: '#FFFFFF', paddingTop: '10%' }]}
            >
              <View style={{ alignSelf: 'flex-start' }}>
                <Text allowFontScaling={false} style={[FontStyle.h1, {}]}>Register</Text>
                <Text allowFontScaling={false} style={[FontStyle.s1, {}]}>Please complete the form below.</Text>
              </View>

              <View style={{ width: '100%', marginTop: 30 }}>

                <Text allowFontScaling={false} style={[FontStyle.formLabel]} category='label'>First Name</Text>
                <Input
                  allowFontScaling={false}
                  style={[Style.formInput]}
                  textStyle={[FontStyle.formInput]}
                  size='large'
                  onChangeText={handleChange('FirstName')}
                  onBlur={() => {
                    setFieldTouched('FirstName');
                  }}
                  status={setInputStatus(touched.FirstName, errors.FirstName)}
                  value={values.FirstName}
                />
                {touched.FirstName && errors.FirstName &&
                  <Text allowFontScaling={false} style={[Style.errorMessageStyle]}>{errors.FirstName}</Text>
                }

                <Text allowFontScaling={false} style={[FontStyle.formLabel]} category='label'>Last Name</Text>
                <Input
                  allowFontScaling={false}
                  style={[Style.formInput]}
                  textStyle={[FontStyle.formInput]}
                  size='large'
                  onChangeText={handleChange('LastName')}
                  onBlur={() => {
                    setFieldTouched('LastName');
                  }}
                  status={setInputStatus(touched.LastName, errors.LastName)}
                  value={values.LastName}
                />
                {touched.LastName && errors.LastName &&
                  <Text allowFontScaling={false} style={[Style.errorMessageStyle]}>{errors.LastName}</Text>
                }

                <Text allowFontScaling={false} style={[FontStyle.formLabel]} category='label'>Email Address</Text>
                <Input
                  allowFontScaling={false}
                  keyboardType="email-address"
                  style={[Style.formInput]}
                  textStyle={[FontStyle.formInput]}
                  size='large'
                  onChangeText={handleChange('EmailAddress')}
                  onBlur={() => {
                    setFieldTouched('EmailAddress');
                  }}
                  status={setInputStatus(touched.EmailAddress, errors.EmailAddress)}
                  value={values.EmailAddress}
                />
                {touched.EmailAddress && errors.EmailAddress &&
                  <Text allowFontScaling={false} style={[Style.errorMessageStyle]}>{errors.EmailAddress}</Text>
                }

                <Text allowFontScaling={false} style={[FontStyle.formLabel]} category='label'>Group Registration ID</Text>
                <Input
                  allowFontScaling={false}
                  keyboardType="name-phone-pad"
                  style={[Style.formInput]}
                  textStyle={[FontStyle.formInput]}
                  size='large'
                  onChangeText={handleChange('Client_AccessCode')}
                  onBlur={() => {
                    setFieldTouched('Client_AccessCode');
                  }}
                  status={setInputStatus(touched.Client_AccessCode, errors.Client_AccessCode)}
                  value={values.Client_AccessCode}
                />
                {touched.Client_AccessCode && errors.Client_AccessCode &&
                  <Text allowFontScaling={false} style={[Style.errorMessageStyle]}>{errors.Client_AccessCode}</Text>
                }

                <Text allowFontScaling={false} style={[FontStyle.formLabel]} category='label'>Hire Date</Text>
                <Input
                  allowFontScaling={false}
                  value={moment(values.HireDate).format('MMMM DD, yyyy')}
                  style={[Style.formInput]}
                  textStyle={[FontStyle.formInput]}
                  placeholder='yyyy-mm-dd'
                  size='large'
                  onFocus={Platform.OS == 'ios' ? openBottomSheetHireDate : showDatePickerHireDate
                  }
                  onBlur={() => {
                    Platform.Os == 'ios' ? closeBottomSheetHireDate :
                      showDatePickerHireDate
                  }}
                  status={setInputStatus(touched.HireDate, errors.HireDate)}

                />
                {touched.HireDate && errors.HireDate &&
                  <Text allowFontScaling={false} style={[Style.errorMessageStyle]}>{errors.HireDate}</Text>
                }

                {Platform.OS === 'ios' ?
                  <BottomSheet
                    ref={refBottomSheetHireDate}
                    height={300}
                    openDuration={100}
                    closeOnDragDown={false}
                    closeOnPressMask={false}
                    customStyles={{ container: { borderTopRightRadius: 10, borderTopLeftRadius: 10 } }}
                    animationType="fade"
                  >
                    <BottomSheetHeader
                      staticTitle="Select an option"
                      touchableTitle="Done"
                      actionOnPress={() => closeBottomSheetHireDate()}
                    >
                      <DateTimePicker
                        testID="dateTimePicker"
                        timeZoneOffsetInMinutes={0}
                        value={values.HireDate}
                        minimumDate={new Date(1950,0,1)}
                        maximumDate={Date.now()}
                        mode={'date'}
                        display='spinner'
                        is24Hour={true}
                        onChange={(event, selectedDate) => {
                          if (selectedDate != undefined) {
                            values.HireDate = selectedDate
                            onChangeHireDate(event, selectedDate)
                          }
                          else {
                            values.HireDate = registerValues.HireDate;
                          }
                        }}
                        onClose={() => refBottomSheetHireDate.current.close()}
                      />
                    </BottomSheetHeader>
                  </BottomSheet> :
                  showHireDate &&
                  <DateTimePicker
                    testID="dateTimePicker"
                    timeZoneOffsetInMinutes={0}
                    minimumDate={new Date(1950,0,1)}
                    maximumDate={Date.now()}
                    value={values.HireDate}
                    mode={'date'}
                    display='spinner'
                    is24Hour={true}
                    onChange={(event, selectedDate) => {

                      if (selectedDate != undefined) {
                        values.HireDate = selectedDate
                        onChangeHireDate(event, selectedDate)
                      }
                      else {
                        values.HireDate = registerValues.HireDate;
                      }
                    }}

                    onClose={setShowHireDate(false)}
                  />
                }

                <Text allowFontScaling={false} style={[FontStyle.formLabel]} category='label'>Full SSN</Text>
                <Input
                  maxLength={11}
                  allowFontScaling={false}
                  style={[Style.formInput]}
                  textStyle={[FontStyle.formInput]}
                  size='large'
                  keyboardType="number-pad"
                  clearButtonMode="while-editing"
                  onChange={(e, elem) => {
                    const event = {
                      persist: () => { },
                      target: {
                        type: "change",
                        id: 'SSN',
                        name: 'SSN',
                        value: FormatSocialSecurity(e.nativeEvent.text)
                      }
                    };
                    handleChange(event);
                  }}
                  onBlur={() => {
                    setFieldTouched('SSN');
                  }}
                  value={values.SSN}
                  status={setInputStatus(touched.SSN, errors.SSN)}
                />


                {touched.SSN && errors.SSN &&
                  <Text allowFontScaling={false} style={[Style.errorMessageStyle]}>{errors.SSN}</Text>
                }

                <Text allowFontScaling={false} style={[FontStyle.formLabel]} category='label'>Date of Birth</Text>
                <Input
                  allowFontScaling={false}
                  value={moment(values.DateOfBirth).format('MMMM DD, yyyy')}
                  style={[Style.formInput]}
                  textStyle={[FontStyle.formInput]}
                  size='large'
                  onFocus={Platform.OS == 'ios' ? openBottomSheetDOB : showDatePickerDOB
                  }
                  onBlur={() => {
                    Platform.Os == 'ios' ? closeBottomSheetDOB :
                      showDatePickerDOB
                  }}
                  status={setInputStatus(touched.DateOfBirth, errors.DateOfBirth)}

                />
                {touched.DateOfBirth && errors.DateOfBirth &&
                  <Text allowFontScaling={false} style={[Style.errorMessageStyle]}>{errors.DateOfBirth}</Text>
                }
                {Platform.OS === 'ios' ?
                  <BottomSheet
                    ref={refBottomSheetDOB}
                    height={300}
                    openDuration={100}
                    closeOnDragDown={false}
                    closeOnPressMask={false}
                    customStyles={{ container: { borderTopRightRadius: 10, borderTopLeftRadius: 10 } }}
                    animationType="fade"
                  >
                    <BottomSheetHeader
                      staticTitle="Select an option"
                      touchableTitle="Done"
                      actionOnPress={() => closeBottomSheetDOB()}
                    >
                      <DateTimePicker
                        testID="dateTimePicker"
                        timeZoneOffsetInMinutes={0}
                        value={values.DateOfBirth}
                        minimumDate={new Date(1950,0,1)}
                        maximumDate={Date.now()}
                        mode={'date'}
                        display='spinner'
                        is24Hour={true}
                        onChange={(event, selectedDate) => {
                          if (selectedDate != undefined) {
                            values.DateOfBirth = selectedDate
                            onChangeDOB(event, selectedDate)
                          }
                          else {
                            values.DateOfBirth = registerValues.DateOfBirth;
                          }
                        }}
                        onClose={() => refBottomSheetDOB.current.close()}
                      />
                    </BottomSheetHeader>
                  </BottomSheet> :
                  showDOB &&
                  <DateTimePicker
                    testID="dateTimePicker"
                    timeZoneOffsetInMinutes={0}
                    value={values.DateOfBirth}
                    minimumDate={new Date(1950,0,1)}
                    maximumDate={Date.now()}
                    mode={'date'}
                    display='spinner'
                    is24Hour={true}
                    onChange={(event, selectedDate) => {

                      if (selectedDate != undefined) {
                        values.DateOfBirth = selectedDate
                        onChangeDOB(event, selectedDate)
                      }
                      else {
                        values.DateOfBirth = registerValues.DateOfBirth;
                      }
                    }}

                    onClose={setShowDOB(false)}
                  />
                }

                <Text allowFontScaling={false} style={[FontStyle.formLabel]} category='label'>Username</Text>
                <Input
                  allowFontScaling={false}
                  autoCapitalize="none"
                  style={[Style.formInput]}
                  textStyle={[FontStyle.formInput]}
                  size='large'
                  onChangeText={handleChange('Username')}
                  onBlur={() => {
                    setFieldTouched('Username');
                  }}
                  status={setInputStatus(touched.Username, errors.Username)}
                  value={values.Username} />
                {touched.Username && errors.Username &&
                  <Text allowFontScaling={false} style={[Style.errorMessageStyle]}>{errors.Username}</Text>
                }

                <Text allowFontScaling={false} style={[FontStyle.formLabel]} category='label'>Create Password</Text>
                <Input
                  allowFontScaling={false}
                  style={[Style.formInput]}
                  textStyle={[FontStyle.formInput]}
                  size='large'
                  accessoryRight={showPassword}
                  secureTextEntry={secureTextEntryPassword}
                  onChangeText={handleChange('Password')}
                  status={setInputStatus(touched.Password, errors.Password)}
                  value={values.Password}
                  onBlur={() => {
                    setFieldTouched('Password');
                  }}
                />
                {touched.Password && errors.Password &&
                  <Text allowFontScaling={false} style={[Style.errorMessageStyle]}>{errors.Password}</Text>
                }

                <Text allowFontScaling={false} style={[FontStyle.formLabel]} category='label'>Confirm Password</Text>
                <Input
                  allowFontScaling={false}
                  style={[Style.formInput]}
                  textStyle={[FontStyle.formInput]}
                  size='large'
                  accessoryRight={showConfirmPassword}
                  secureTextEntry={secureTextEntryCPassword}
                  onChangeText={handleChange('ConfirmPassword')}
                  onBlur={() => {
                    setFieldTouched('ConfirmPassword');
                  }}
                  status={setInputStatus(touched.ConfirmPassword, errors.ConfirmPassword)}
                  value={values.ConfirmPassword}
                />

                {touched.ConfirmPassword && errors.ConfirmPassword &&
                  <Text allowFontScaling={false} style={[Style.errorMessageStyle]}>{errors.ConfirmPassword}</Text>
                }

              </View>

              <View style={{ marginBottom: Platform.OS === 'ios' ? 60 : 80, width: '100%' }}>
                <Button
                  buttonText="Register"
                  buttonColor={Color.greenBg}
                  buttonStyle={[Style.roundButton]}
                  textStyle={{ textTransform: "uppercase", fontFamily: "OpenSans-Bold", fontSize: 22, letterSpacing: 0.5 }}
                  activeOpacity={isSubmitting && isValid ? 1 : 0.5}
                  spinnerActive={isSubmitting && isValid}
                  actionOnPress={() => handleSubmit()}
                />
              </View>

            </KeyboardAwareScrollView>
          )}
        </Formik>
      </SafeAreaView>
    </>
  );
}

const mapDispatchToProps = {
  ShowToast
};

const styles = StyleSheet.create({
  input: {
    flex: 1,
    margin: 2,
  },
  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  controlContainer: {
    borderRadius: 4,
    margin: 2,
    padding: 6,
    backgroundColor: '#3366FF',
  },
});


export default connect(null, mapDispatchToProps)(Register);