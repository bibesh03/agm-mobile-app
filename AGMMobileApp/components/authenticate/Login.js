import React, { useState, useEffect } from 'react';
import { StyleSheet, View, SafeAreaView, Platform, Keyboard, StatusBar } from 'react-native';
import { Formik } from 'formik';
import Validator from '../../_helpers/LoginValidator';
import Style from '../../assets/styles/styles';
import Color from '../../assets/styles/colors';
import FontStyle from '../../assets/styles/font';
import Button from '../shared/Button';
import { Text, Input, Icon, Spinner } from '@ui-kitten/components';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { authenticate, storeToken, getToken,getPushNotificationToken } from '../../_services/authenticateService';
import { ShowToast } from '../../_actions/toast.action';
import { connect } from 'react-redux';
import * as Animatable from 'react-native-animatable';


const Login = (props) => {
  const [loginValues, setLoginValues] = useState({ username: '', password: '', pushNotificationToken:'' });
  const { ShowToast } = props;
  const [pushNotificationToken, setPushNotificationToken] = useState(null);

  useEffect(() => {
    const getPushNotificationTokenHere = async () => {
        setPushNotificationToken(await getPushNotificationToken());
    }
    getPushNotificationTokenHere();
  }, []);

  // USER INPUT ICON
  const userIcon = (props) => (
    <Icon
      {...props}
      fill='#8F9BB3'
      name='at-outline'
    />
  );

  const LoginUser = (model, setSubmitting) => {
    Keyboard.dismiss();
    authenticate(model).then(res => {
      if (res.token && res.isAuthenticated) {
        storeToken(res);
        props.navigation.reset({
          index: 0,
          routes: [{ name: 'AuthStackScreen', params: { token: res.token } }]
        });
        getToken();
      }
      else {
        ShowToast({ 'message': res.Message, 'type': res.Success ? 'success' : 'danger' });
      }
      setSubmitting(false);
    }).catch(err => {
      ShowToast({ 'message': 'Network Error! Please try again!', 'type': 'danger' });
      setSubmitting(false);
    });

  }
  const setInputStatus = (touched, hasError) => {
    if (hasError) {
      return 'danger';
    } else if (touched && !hasError) {
      return 'success';
    } else {
      return ''
    }
  };

  // PASSWORD INPUT ICON
  const [secureTextEntry, setSecureTextEntry] = React.useState(true);
  const toggleSecureEntry = () => {
    setSecureTextEntry(!secureTextEntry);
  }

  const showPassword = (props) => (
    <TouchableWithoutFeedback onPress={toggleSecureEntry}>
      <Icon {...props} name={secureTextEntry ? 'eye-off-outline' : 'eye-outline'} />
    </TouchableWithoutFeedback>
  );

  // LOADING INDICATOR
  const loadingIndicator = (props) => (
    <View style={[props.style]}>
      <Spinner status='basic' size='medium' />
    </View>
  );

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <Formik
        initialValues={loginValues}
        validationSchema={Validator}
        onSubmit={(values, { setSubmitting }) => {
          values.pushNotificationToken = pushNotificationToken;
          setLoginValues(values);
          LoginUser(values, setSubmitting);
          // signIn({values});
          setSubmitting(true);
        }}
      >
        {({ handleChange, handleBlur, handleSubmit, values, errors, touched, isValid, setFieldTouched, isSubmitting }) => (
          <SafeAreaView style={[Color.whiteBg, Style.mainContainer]}>
            <View style={{ ...StyleSheet.absoluteFill }}>
            </View>

            <Animatable.View
              animation="fadeInUp"
              duration={300}
              delay={600}
              iterationCount={1}
              style={[Style.flexContainer, Style.containerPaddingMed, Color.whiteBg, { paddingTop: '20%' }]}
            >
              <View style={{ alignSelf: 'flex-start' }}>
                <Text allowFontScaling={false} style={[FontStyle.h1, {}]}>Sign in</Text>
                <Text allowFontScaling={false} style={[FontStyle.s1, {}]}>Please log in to continue.</Text>
              </View>

              <View style={{ width: '100%', marginTop: 30 }}>

                <Input
                  allowFontScaling={false}
                  style={[Style.formInput]}
                  textStyle={[FontStyle.formInput]}
                  size='large'
                  placeholder='Username'
                  keyboardType='email-address'
                  autoCapitalize="none"
                  onChangeText={handleChange('username')}
                  status={setInputStatus(touched.username, errors.username)}
                  accessoryRight={userIcon}
                  onBlur={() => {
                    setFieldTouched('username');
                  }}
                  value={values.username}
                />
                {touched.username && errors.username && <Text allowFontScaling={false} style={[Style.errorMessageStyle]}>{errors.username}</Text>}
                <Input
                  allowFontScaling={false}
                  style={[Style.formInput]}
                  textStyle={[FontStyle.formInput]}
                  size='large'
                  placeholder='Password'
                  autoCapitalize="none"
                  accessoryRight={showPassword}
                  secureTextEntry={secureTextEntry}
                  status={setInputStatus(touched.password, errors.password)}
                  onChangeText={handleChange('password')}
                  onBlur={() => {
                    setFieldTouched('password');
                  }}
                  value={values.password}
                />
                <View style={{ width: '100%' }}>
                  {touched.password && errors.password &&
                    <Text allowFontScaling={false} style={[Style.errorMessageStyle]}>{errors.password}</Text>
                  }
                </View>
              </View>
              <View style={{ width: '100%' }}>
                <Button
                  buttonText="Login"
                  buttonColor={Color.greenBg}
                  buttonStyle={[Style.roundButton]}
                  textStyle={{ textTransform: "uppercase", fontFamily: "OpenSans-Bold", fontSize: 22, letterSpacing: 0.5 }}
                  activeOpacity={isSubmitting ? 1 : 0.5}
                  spinnerActive={isSubmitting}
                  actionOnPress={isSubmitting && isValid ? () => { } : () => handleSubmit()}
                />
              </View>

              <View style={{ position: 'absolute', bottom: '5%' }}>
                <Text
                  allowFontScaling={false}
                  style={[FontStyle.p2, Color.greyColor]}
                >
                  Don't have an account?
                <Text
                    allowFontScaling={false}
                    onPress={() => { props.navigation.navigate('Register') }}
                    style={[FontStyle.p2, Color.greenColor, { fontFamily: 'OpenSans-Bold' }]}
                  > Register here
                </Text>
                </Text>
              </View>

            </Animatable.View>
          </SafeAreaView>
        )}
      </Formik>
    </>
  );
}

const mapDispatchToProps = {
  ShowToast
};

export default connect(null, mapDispatchToProps)(Login);