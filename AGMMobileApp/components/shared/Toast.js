import React, { useEffect, useState, useRef } from 'react';
import { TouchableOpacity, View, Animated, Easing, Platform } from 'react-native';
import { Text } from '@ui-kitten/components';
import { Icon } from 'react-native-elements';
import Style from '../../assets/styles/styles';
import Color from '../../assets/styles/colors';
import { connect } from 'react-redux';
import { HideToast } from '../../_actions/toast.action';


const Toast = (props) => {
    const { toastState, HideToast } = props;
    const [borderColor, setBorderColor] = useState({
        "danger": "#FF1F1F",
        "success": '#00AF54',
        "warning": '#FF7700',
    });

    useEffect(() => {
        if (toastState.isVisible) {
            SlideDown();
            return;
        }

        SlideUp();
    }, [toastState]);

    const translateY = useRef(new Animated.Value(0)).current;
    
    const SlideUp = () => {
        Animated.timing(translateY, {
            toValue: -500,
            duration: 300,
            useNativeDriver: true

        }).start();
    }

    const SlideDown = () => {
        Animated.timing(translateY, {
            toValue: Platform.OS === 'ios' ? 0 : -10,
            duration: 600,
            easing: Easing.elastic(1),
            useNativeDriver: true

        }).start(({ finished }) => {
            setTimeout(() => {
                SlideUp()
            }, 3000);
        })
    }

    const DismissToast = () => {
        SlideUp();
        HideToast();
    }

    return (
        <>
            <Animated.View style={[ Style.toastContainer, { transform: [{ translateY: translateY }] } ]}>
                <View style={[Color.lightestBg, Style.toast, { borderLeftColor: borderColor[toastState.type] }]}>
                    <View style={{ flex: 7, justifyContent: 'center' }}>
                        <Text allowFontScaling={false} style={[Color.blackColor, { justifyContent: 'flex-start', fontSize: 16 }]}>
                            {toastState.message}
                        </Text>
                    </View>
                    <View style={{ flex: 1 }}>
                        {Platform.OS === 'ios' ?
                            <TouchableOpacity
                            activeOpacity={0.5} 
                            onPress={() => DismissToast()}
                            >
                            <Icon
                                allowFontScaling={false}
                                name='x'
                                type='feather'
                                size={25}
                                color={Color.greyColor.color}
                                iconStyle={{ opacity: 0.8 }}
                            />
                            </TouchableOpacity> : <></>
                        }
                    </View>
                </View>
            </Animated.View>
        </>
    );
}

const mapStateToProps = (state) => ({
    toastState: state.ToastReducers.ToastReducer
});

const mapDispatchToProps = {
    HideToast
};

export default connect(mapStateToProps, mapDispatchToProps)(Toast);