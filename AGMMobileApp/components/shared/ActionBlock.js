import React, { useState, useEffect } from 'react';
import { SafeAreaView, View, TouchableOpacity } from 'react-native';
import { Text, List } from '@ui-kitten/components';
import { Icon } from 'react-native-elements';
import Style from '../../assets/styles/styles';
import Color from '../../assets/styles/colors';
import Card from '../../assets/styles/card';

const ActionBlock = (props) => {

    return (
        <TouchableOpacity 
            activeOpacity={0.8}
            // onPress={() => props.navigation.navigate(props.goto)}
            onPress={props.actionOnPress} 
            style={[ Card.base, Style.actionBlock, Color.whiteBg, props.actionBlockStyle ]}
        >
            <View style={[ Style.actionBlockTitleContainer ]}>
                <Text allowFontScaling={false} style={[  Style.actionBlockTitle, props.titleStyle ]}>{props.title}</Text>
            </View>

            <Icon
                allowFontScaling={false} 
                name={props.iconRight} 
                type='feather' 
                size={23} 
                iconStyle={[ Color.blackColor, { opacity: 0.7 } ]} 
            />
        </TouchableOpacity>
    )
}

export default ActionBlock;