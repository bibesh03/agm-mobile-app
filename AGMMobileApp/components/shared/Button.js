import React, { useState, useEffect } from 'react';
import { SafeAreaView, View, TouchableOpacity } from 'react-native';
import { Text, Spinner } from '@ui-kitten/components';
import ButtonStyle from '../../assets/styles/button';
import Color from '../../assets/styles/colors';


const Button = (props) => {

    return (
        <TouchableOpacity
            activeOpacity={props.activeOpacity ? props.activeOpacity : 0.5}
            onPress={props.actionOnPress && !props.spinnerActive ? props.actionOnPress : () => { }}
            style={[ButtonStyle.baseBottomSheetBtn, props.buttonColor, props.buttonStyle]}>
            {
                props.spinnerActive ?
                    <Spinner status="basic" size="large" />
                    :
                    <Text allowFontScaling={false} style={[ButtonStyle.baseBottomSheetBtnText, Color.whiteColor, props.textStyle]}>{props.buttonText}</Text>

            }
        </TouchableOpacity>
    )
}


export default Button;