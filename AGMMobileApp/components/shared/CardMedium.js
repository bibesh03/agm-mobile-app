import React, { useState, useEffect } from 'react';
import { TouchableOpacity, View } from 'react-native';
import { Text } from '@ui-kitten/components';
import { Icon } from 'react-native-elements';
import Card from '../../assets/styles/card';
import FontStyle from '../../assets/styles/font';

const CardMedium = (props) => {

    return (
        <View>
            <TouchableOpacity
                activeOpacity={props.actionOnPress ? 0.5 : 1}
                onPress={props.actionOnPress ? props.actionOnPress : () => { }}
                style={[Card.base, Card.medium, props.cardMediumStyle]}
            >
                <View style={[Card.contentContainer]}>
                    <Text allowFontScaling={false} style={[FontStyle.cardMediumTitle, props.titleStyle]}>
                        {props.header}
                    </Text>

                    <Text allowFontScaling={false} style={[FontStyle.cardMediumDescription, props.descriptionStyle]}>
                        {props.description}
                    </Text>
                </View>

                <TouchableOpacity
                    activeOpacity={props.actionIconOnPress ? 0.5 : 1}
                    onPress={props.actionIconOnPress ? props.actionIconOnPress : () => { }}
                    style={[Card.iconTopRightContainer]}
                >
                    {
                        props.actionIconOnPress ?
                            <Icon
                                name={props.icon}
                                type='feather'
                                size={props.iconSize ? props.iconSize : 22}
                                style={[]}
                                iconStyle={[props.iconStyle]}
                            /> : <></>
                    }
                </TouchableOpacity>
            </TouchableOpacity>
        </View>
    )
}

export default CardMedium;