import React,{useState, useEffect} from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import DocumentScreen from '../NavStack/TabNavigation/DocumentScreen';
import MediaScreen from '../NavStack/TabNavigation/MediaScreen';

const TabNav = createBottomTabNavigator();

const Tabscreen = (props) => {
       const {documents} = props;
    return (
        <TabNav.Navigator  
          
            tabBarOptions= {{
                activeTintColor: 'red', 
                inactiveTintColor: 'grey'
            }}>
                {/* Separating documents into two different tabs based on their type/Extension (either pdf or not */}
            <TabNav.Screen name='DocumentView' children={() => <DocumentScreen nonMediaDocuments={documents.filter(element=> element.FileExtension=== '.pdf')} clientId={props.clientId} {...props}/>} options={{
                tabBarLabel:'Documents',
                // tabBarIcon:({tintColor}) => (
                //     <Icon name='ios-document' color={tintColor} size={25} />
                // )
            }}></TabNav.Screen>
            <TabNav.Screen name='MediaView' children={()=><MediaScreen mediaDocuments={documents.filter(element=> element.FileExtension !== '.pdf')} clientId={props.clientId} {...props}/>} options={{
                tabBarLabel:'Media',
                // tabBarIcon:({tintColor}) => (
                //     <Icon name='ios-image' color={tintColor} size={25} />
                // )
            }}></TabNav.Screen>
            </TabNav.Navigator>
    )
}
export default Tabscreen;
