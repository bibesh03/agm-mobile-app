import React, { useEffect, useState, useRef } from 'react';
import { View, TouchableOpacity } from'react-native';
import { Text } from '@ui-kitten/components';
import { Icon } from 'react-native-elements';
import Style from '../../assets/styles/styles';
import Color from '../../assets/styles/colors';


const ModalHeader = (props) => {

    return (
        <View style={[Style.titleBlockBorder, props.titleBlockContainerStyle, { paddingBottom: 10 }]}>
            <View style={[Style.containerPadding, { flexDirection: 'row', alignItems: 'center', }]}>
                <TouchableOpacity
                    activeOpacity={props.actionOnPressLeft ? 0.5 : 1}
                    onPress={props.actionOnPressLeft ? props.actionOnPressLeft : () => {}}
                    style={[ Style.modalHeaderLeftElementContainer ]}
                >

                    {props.textLeft ? 
                        <Text allowFontScaling={false} style={[Color.lightBlueColor, { fontSize: 18, fontWeight: '500' }]}>{props.textLeft}</Text> : <></>
                    }

                    {props.iconLeft ?
                        <Icon 
                            name={props.iconLeft} 
                            type="feather" 
                            size={25}
                            iconStyle={[ props.iconLeftStyle ]}
                        /> : <></>
                    } 

                </TouchableOpacity>

                <View style={[ Style.modalHeaderCenterElementContainer ]}>
                    <Text allowFontScaling={false} style={[ Style.modalHeaderTitle ]}>{props.title}</Text>
                </View>

                <TouchableOpacity 
                    activeOpacity={props.actionOnPressRight ? 0.5 : 1}
                    onPress={props.actionOnPressRight ? props.actionOnPressRight : () => {}} 
                    style={[ Style.modalHeaderRightElementContainer ]} 
                >
                    {props.iconRight || props.dismissIcon ?
                        <Icon 
                            name={props.dismissIcon ? 'x' : props.iconRight} 
                            type="feather" 
                            size={props.dismissIcon ? 22 : 25}
                            containerStyle={[ props.dismissIcon ? Style.modalHeaderDismissIconContainer : "" ]}
                            iconStyle={[ props.dismissIcon ? Style.modalHeaderDismissIcon : props.iconRightStyle ]}
                        /> : <></>
                    }

                    {props.textRight ?
                        <Text allowFontScaling={false} style={[Color.lightBlueColor, { fontSize: 18, fontWeight: '500', fontFamily: 'OpenSans-Regular' }]}>{props.textRight}</Text> : <></>
                    }
                </TouchableOpacity>
            </View>
        </View>
    );
}

export default ModalHeader;