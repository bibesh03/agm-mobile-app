import React, { useState, useEffect } from 'react';
import { SafeAreaView, View, TouchableOpacity, TouchableOpacityComponent } from 'react-native';
import { Text, Toggle } from '@ui-kitten/components';
import { Icon } from 'react-native-elements';
import Style from '../../assets/styles/styles';
import Color from '../../assets/styles/colors';

const ListBlock = (props) => {

  const [checked, setChecked] = React.useState(false);

  const onCheckedChange = (isChecked) => {
    setChecked(isChecked);
  };

  return (
    <TouchableOpacity
      activeOpacity={props.actionOnPress ? 0.5 : 1}
      onPress={props.actionOnPress ? props.actionOnPress : () => { }}
      style={[Style.listBlock, props.listBlockStyle,]}>

      <View style={[
        Style.listBlockTitleContainer,
        props.fullWidthTitle ? { width: '100%' } : {},
        props.hasIndicator ? { width: '70%' } : {},
        props.textRight ? { width: '29%' } : {}]}
      >
        {props.hasIndicator ?
          <View style={[props.indicatorColor, { height: 8, width: 8, borderRadius: '100%', marginRight: 10 }]}></View> : <></>

        }

        <Icon
          allowFontScaling={false}
          name={props.iconLeft}
          type="feather"
          size={25}
          iconStyle={[props.iconLeftColor ? props.iconLeftColor : Color.greenColor, { marginRight: props.iconLeft ? 10 : 0 }]}
        />

        <View>
          {props.actionTitleOnPress ?
            <TouchableOpacity
              activeOpacity={props.actionTitleOnPress ? 0.5 : 1}
              onPress={props.actionTitleOnPress ? props.actionTitleOnPress : () => { }}
            >
              <Text allowFontScaling={false} style={[Style.listBlockTitle, props.titleStyle,]}>{props.title}</Text>
            </TouchableOpacity> :

            <Text allowFontScaling={false} style={[Style.listBlockTitle, props.titleStyle,]}>{props.title}</Text>}

          {props.subtitle ? <Text allowFontScaling={false} style={[Style.listBlockSubtitle, props.subtitleStyle]}>{props.subtitle}</Text> : <></>}
          {
            props.linkText ?
              <TouchableOpacity
                activeOpacity={props.actionLinkOnPress ? 0.5 : 1}
                onPress={props.actionLinkOnPress ? props.actionLinkOnPress : () => { }}
              >
                <Text allowFontScaling={false} style={[Style.listBlockSubtitleAlt, props.linkTextStyle]} >{props.linkText}</Text>
              </TouchableOpacity> : <></>
          }
        </View>
      </View>

      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
        <View style={[Style.listBlockBadge, props.badgeColor]}>
          <Text allowFontScaling={false} style={[Style.listBlockBadgeText]}>{props.badgeStatus}</Text>
        </View>

        {props.toggle ?
          <View>
            <Toggle
              checked={checked}
              onChange={onCheckedChange}
              size="small"
            >
            </Toggle>
          </View>
          : <></>
        }

        <Text>{props.textRight}</Text>

        <TouchableOpacity
          activeOpacity={props.iconActionOnPress ? 0.5 : 1}
          onPress={props.iconActionOnPress ? props.iconActionOnPress : () => { }}
          style={[props.iconRightBackground, props.iconRightBackground ? Style.listBlockIconRightContainer : ""]}
        >
          <Icon
            allowFontScaling={false}
            name={props.iconRight}
            size={props.iconRightSize}
            type={props.iconRightType ? props.iconRightType : "feather"}
            iconStyle={[props.iconRightColor ? props.iconRightColor : Color.darkGreyColor, props.iconRight == "chevron-right" ? { marginRight: -5, marginTop: 3, color: Color.midGreyColor.color } : {}, { opacity: 0.8 }]}
          />
        </TouchableOpacity>
      </View>
    </TouchableOpacity>
  )
}

export default ListBlock;