import React, { useState, useEffect } from 'react';
import { TouchableOpacity, View } from 'react-native';
import { Text } from '@ui-kitten/components';
import { Icon } from 'react-native-elements';
import Card from '../../assets/styles/card';
import FontStyle from '../../assets/styles/font';
import * as Animatable from 'react-native-animatable';

const CardSmall = (props) => {

    return (
        <Animatable.View
            animation="zoomIn"
            duration={300}
            delay={300}
            iterationCount={1}
        >
            <TouchableOpacity 
                activeOpacity={props.actionOnPress ? 0.5 : 1}
                onPress={props.actionOnPress ? props.actionOnPress : () => {}} 
                style={[ Card.base, Card.small ]}
                >
                <View style={[ Card.contentContainer ]}>
                    <Text allowFontScaling={false} style={[ FontStyle.cardSmallTitle, {  } ]}>
                    {props.header}
                    </Text>

                    <Text allowFontScaling={false} style={[ FontStyle.cardSmallDescription, {  } ]}>
                    {props.description}
                    </Text>
                </View>

                <TouchableOpacity
                    activeOpacity={props.actionIconOnPress ? 0.5 : 1}
                    onPress={props.actionIconOnPress ? props.actionIconOnPress : () => {}} 
                    style={[ Card.iconTopRightContainer ]}
                >
                    <Icon 
                        name={props.icon} 
                        type='feather' 
                        size={25} 
                        style={[ Card.iconClose ]} 
                    />
                </TouchableOpacity>
            </TouchableOpacity>
        </Animatable.View>
    )
}

export default CardSmall;