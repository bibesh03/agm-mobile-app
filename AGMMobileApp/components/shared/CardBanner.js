import React, { useState, useEffect } from 'react';
import { TouchableOpacity, View } from 'react-native';
import { Text, Button } from '@ui-kitten/components';
import { Icon } from 'react-native-elements';
import Card from '../../assets/styles/card';
import FontStyle from '../../assets/styles/font';
import Color from '../../assets/styles/colors';

const CardBanner = (props) => {

    return (
        <View>
            <View style={[ Card.base, Card.banner ]}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={[ Card.bannerNumBlock ]}>
                        <Text allowFontScaling={false} style={[ FontStyle.cardBannerNumber ]}>
                            {props.number}
                        </Text>
                    </View>

                    <View style={[ Card.bannerDescriptionBlock ]}>
                        <Text allowFontScaling={false} style={[ FontStyle.cardBannerSubtitle ]}>
                            {props.subtitle}
                        </Text>
                    </View>
                </View>

                <View>
                    <Text allowFontScaling={false} style={[ FontStyle.cardBannerDescription, {  } ]}>
                        {props.description}
                    </Text>
                </View>
                
                <View style={{ alignItems: 'flex-end', marginTop: 15 }}>
                    <TouchableOpacity
                        activeOpacity={props.actionOnPress ? 0.5 : 1}
                        onPress={props.actionOnPress ? props.actionOnPress : () => {}}
                        style={[ Color.greenBg, { paddingVertical: 15, paddingHorizontal: 30, borderRadius: 5 } ]}
                    >
                        <Text allowFontScaling={false} style={[ FontStyle.cardButton ]}>
                            {props.buttonText}
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

export default CardBanner;