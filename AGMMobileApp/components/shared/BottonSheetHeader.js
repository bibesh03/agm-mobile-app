import React, { useEffect, useState, useRef } from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import Style from '../../assets/styles/styles'
import Color from '../../assets/styles/colors';

const BottomSheetHeader = (props) => {

    return (
        <View style={[Style.mainContainer, { paddingTop: '5%' }]}>
            <View style={[ Style.bottomSheetTitleBorder ]}>
                <View style={[ Style.containerPadding, Style.bottomSheetTitleElementContainer ]}>
                    <View>
                        <Text allowFontScaling={false} style={[ Style.bottomSheetStaticTitleText, Color.darkGreyColor ]}>
                            {props.staticTitle}
                        </Text>
                    </View>

                    <TouchableOpacity
                        activeOpacity={props.actionOnPress ? 0.5 : 1}
                        onPress={props.actionOnPress ? props.actionOnPress : () => {}}
                    >
                        <Text allowFontScaling={false} style={[ Style.bottomSheetTouchableTitleText ]}>
                            {props.touchableTitle}
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>

            {props.children}
        </View>
    );
}

export default BottomSheetHeader;