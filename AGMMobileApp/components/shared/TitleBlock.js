import React, { useState, useEffect } from 'react';
import { SafeAreaView, View, TouchableOpacity } from 'react-native';
import { Text, List } from '@ui-kitten/components';
import { Icon } from 'react-native-elements';
import Style from '../../assets/styles/styles';
import Color from '../../assets/styles/colors';
import Card from '../../assets/styles/card';

const TitleBlock = (props) => {

    return (
        <View style={[ Style.titleBlockBorder ]}>
            <View style={[ Style.titleBlock, Style.containerPadding, props.titleBlockStyle ]}>
                <View style={[ Style.titleBlockContainer, { flexDirection: 'row', width: props.fullWidthTitle ? '100%' : '80%'} ]}>
                    <Text allowFontScaling={false} style={[ Style.titleBlockText, props.titleBlockTextStyle ]}>{props.title}</Text>
                    <TouchableOpacity
                        activeOpacity={props.actionOnPressTitleIcon ? 0.5 : 1}
                        onPress={props.actionOnPressTitleIcon ? props.actionOnPressTitleIcon : () => {}}
                    >
                        <Icon
                            allowFontScaling={false} 
                            name={props.titleIcon} 
                            type='feather' 
                            size={props.titleIconSize}
                            containerStyle={{ marginLeft: 10 }} 
                            iconStyle={[ Color.darkBlueColor ]} 
                        />
                    </TouchableOpacity>
                </View>
                <TouchableOpacity 
                    activeOpacity={props.actionOnPress ? 0.5 : 1}
                    onPress={props.actionOnPress ? props.actionOnPress : () => {}}
                    style={{ marginTop: -5 }}
                    style={[ props.iconRightContainerStyle ]}
                >
                    <Icon
                        allowFontScaling={false} 
                        name={props.iconRight} 
                        type='feather' 
                        size={props.iconRightSize} 
                        iconStyle={[ Color.darkBlueColor, props.iconRightColor ]} 
                    />
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default TitleBlock;