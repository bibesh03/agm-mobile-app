import React, { useState, useEffect } from 'react';
import { ScrollView } from 'react-native-gesture-handler';
import { SafeAreaView, TouchableOpacity, Alert, View, Linking, Image, Platform } from 'react-native';
import Style from '../../../assets/styles/styles';
import Color from '../../../assets/styles/colors';
import ListBlock from '../../shared/ListBlock';
import TitleBlock from '../../shared/TitleBlock';
import { Text } from '@ui-kitten/components';
import moment from 'moment';
import _ from 'lodash';
import constants from '../../../_constants/constants';
import Loading from '../../../_helpers/Loading';
import { deleteUserDocument, getUserDocuments } from '../../../_services/documentsService';
import { Capitalize, GroupByMonth } from '../../../_helpers/helper';
import * as Animatable from 'react-native-animatable';
import { SwipeRow } from 'react-native-swipe-list-view';
import { ShowToast } from '../../../_actions/toast.action';
import { connect } from 'react-redux';
import * as WebBrowser from 'expo-web-browser';

const MediaScreen = (props) => {
    const { clientId, refresh } = props;
    const { ShowToast, navigation } = props;
    const [mediaDocuments, setMediaDocuments] = useState({});
    const [isLoading, setIsLoadingScreen] = useState(true);

    useEffect(() => {
        GetUserDocuments();
    }, [refresh]);

    const GetUserDocuments = () => {
        getUserDocuments(true).then(res => {
            setMediaDocuments(GroupByMonth(res));
            setIsLoadingScreen(false);
        }).catch(ex => setIsLoadingScreen(false))
    }

    const deleteDocument = (item) => {
        deleteUserDocument(item.GeneralDocumentId, item.Id).then(res => {
            GetUserDocuments();
            ShowToast({ 'message': res.Message, 'type': res.Success ? 'success' : 'warning', "position": "bottom" });
        }
        ).catch(err => {
            console.log(err);
        })
    }
    const DeleteMessageAlert = (item) => {
        Alert.alert(
            "Delete Image",
            "Are you sure you want to delete this image?",
            [
                {
                    text: "Cancel",
                    style: 'destructive'
                },
                {
                    text: "OK",
                    onPress: () => {
                        deleteDocument(item)
                    },
                    style: "cancel"
                }
            ],
            { cancelable: false }
        );
    }

    const _handleViewFileExpoBrowser = async (item) => {
        await WebBrowser.openBrowserAsync(encodeURI(constants.API_URL+`ViewFile?docId=${item.Id}&fileExtension=${item.FileExtension}&fileName=${item.FileName}&clientId=${clientId}`))
        .then(res=>{
        }).catch(err=>{
          console.log(err);
          
        });
    };
    // const _handleDownloadFileExpoBrowser = async (item) => {
    //     await WebBrowser.openBrowserAsync(encodeURI(constants.API_URL + `Download?docId=${item.Id}&fileExtension=${item.FileExtension}&fileName=${item.FileName}&clientId=${clientId}`));
    // };


    return (
        <SafeAreaView style={[Color.lighterBg, Style.mainContainer]}>
            <ScrollView style={[{}]}>
                {
                    isLoading ? <Loading /> :
                        (_.isEmpty(mediaDocuments) ?
                            <Animatable.View
                                animation="fadeInUp"
                                duration={300}
                                delay={300}
                                iterationCount={1}
                                style={[Color.whiteBg, Style.containerPadding, { marginTop: 40 }]}
                            >
                                <ListBlock
                                    title="No Media Available"
                                />
                            </Animatable.View>
                            :
                            Object.keys(mediaDocuments).map((key, i) => {
                                return (
                                    <Animatable.View
                                        animation={Platform.OS === "android" ? null : "fadeInUp"}
                                        duration={300}
                                        delay={300}
                                        iterationCount={1}
                                        key={i}
                                        style={[Color.whiteBg]}
                                    >
                                        <TitleBlock
                                            key={i}
                                            title={Capitalize(key)}
                                            titleBlockStyle={[Color.lighterBg, { paddingTop: 15 }]}
                                            titleBlockTextStyle={[Color.blackColor, { fontSize: 14, fontFamily: 'OpenSans-SemiBold' }]}
                                        />
                                        {
                                            mediaDocuments[key].map((item, i) =>
                                                <SwipeRow
                                                    key={i}
                                                    disableRightSwipe={true}
                                                    rightOpenValue={-80}
                                                >
                                                    <TouchableOpacity
                                                        activeOpacity={0.5}
                                                        onPress={() => DeleteMessageAlert(item)}
                                                        style={[Style.swipeListActionContainer, Color.redBg]}>
                                                        <Text allowFontScaling={false} style={[Style.swipeListActionText, Color.whiteColor, {}]}>
                                                            Delete
                                          </Text>
                                                    </TouchableOpacity>
                                                    <View style={[Color.whiteBg]}>
                                                        <ListBlock
                                                            key={i}
                                                            title={item.FileName}
                                                            subtitle={moment(item.UpdatedOn).format('ddd, MMM D, h:mm a')}
                                                            iconLeft="image"
                                                            iconLeftColor={Color.lightGreenColor}
                                                            iconRight="download"
                                                            iconRightColor={Color.blueColor}
                                                            // actionTitleOnPress={() => _handleViewFileExpoBrowser(item)}
                                                            // iconActionOnPress={() => _handleDownloadFileExpoBrowser(item)}
                                                            actionOnPress={() => _handleViewFileExpoBrowser(item)}
                                                            listBlockStyle={[Style.containerPadding]}
                                                        />
                                                    </View>
                                                </SwipeRow>

                                            )
                                        }
                                    </Animatable.View>
                                )
                            }))}

            </ScrollView>
        </SafeAreaView>
    )
}


const mapDispatchToProps = {
    ShowToast
};
export default connect(null, mapDispatchToProps)(MediaScreen);

