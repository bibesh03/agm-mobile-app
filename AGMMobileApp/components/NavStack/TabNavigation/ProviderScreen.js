import React, { useState, useEffect } from 'react';
import { ScrollView } from 'react-native-gesture-handler';
import { SafeAreaView, View, Linking, Image, Group } from 'react-native';
import Style from '../../../assets/styles/styles';
import Color from '../../../assets/styles/colors';
import ListBlock from '../../shared/ListBlock';
import TitleBlock from '../../shared/TitleBlock';
import moment from 'moment';
import _ from 'lodash';
import constants from '../../../_constants/constants';
import * as WebBrowser from 'expo-web-browser';
import Loading from '../../../_helpers/Loading';
import { getProviders } from '../../../_services/documentsService';
import { Capitalize} from '../../../_helpers/helper';
import * as Animatable from 'react-native-animatable';
import { GetClientID } from '../../../_services/authenticateService';

const ProviderScreen = (props) => {
    const { clientId, navigation } = props;
    const [providers, setProviders] = useState([]);
    const [isLoading, setIsLoadingScreen] = useState(true);
    const [result, setResult] = useState(null);

    useEffect(() => {
        getProviders().then(res => {
            setProviders(res);
            setIsLoadingScreen(false);
        }).catch(ex => { console.log('here',ex);
        setIsLoadingScreen(false)})
    }, []);

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            getProviders().then(res => {
                setProviders(res);
                setIsLoadingScreen(false);
            }).catch(ex => setIsLoadingScreen(false))
        });
        return unsubscribe;
    }, [navigation]);

    const _handlePressButtonAsync = async (item) => {
        await WebBrowser.openBrowserAsync(encodeURI(constants.API_URL + `Download?docId=${item.Id}&fileExtension=${item.FileExtension}&fileName=${item.FileName}&clientId=${clientId}`));
    };

    return (
        <SafeAreaView style={[Color.lighterBg, Style.mainContainer]}>
            <ScrollView style={[{}]}>
        {
            isLoading ? <Loading /> :
            (_.isEmpty(providers) ?
            <Animatable.View
                animation="fadeInUp"
                duration={300}
                delay={300}
                iterationCount={1}
                style={[Color.whiteBg, Style.containerPadding, { marginTop: 40 }]}
            >
                <ListBlock
                    title="No Providers Available"
                />
            </Animatable.View>
            :
                providers.map((item,i) => {
                    return (
                        <Animatable.View
                            animation="fadeInUp"
                            duration={300}
                            delay={300}
                            iterationCount={1}
                            key={i}
                            style={[Color.whiteBg]}
                        >
                            <TitleBlock
                                key={i}
                                title={Capitalize(item.ProviderType)}
                                titleBlockStyle={[Color.lighterBg, { paddingTop: 15 }]}
                                titleBlockTextStyle={[Color.blackColor, { fontSize: 14, fontFamily: 'OpenSans-SemiBold' }]}
                            />
                            {
                                _.orderBy(item.ProviderInfoes, 'SortOrder', 'asc' ).map((info, i) => {

                                    return (
                                        <ListBlock
                                            key={i}
                                            title={info.ProviderName}
                                            iconLeft="users"
                                            iconLeftColor={Color.lightGreenColor}
                                            iconRight="chevron-right"
                                            actionOnPress={() => props.navigation.navigate('ProviderInfo', {providerInfo: info, title: item.ProviderType})}
                                            listBlockStyle={[Style.containerPadding]}
                                        />
                                    )
                                })
                            }
                        </Animatable.View>
                    )
                }))} 
            </ScrollView>
        </SafeAreaView>
    )
}

export default ProviderScreen;
