import React, { useState, useEffect } from 'react';
import { ScrollView } from 'react-native-gesture-handler';
import { SafeAreaView, Linking, TouchableOpacity, Alert, View, Platform } from 'react-native';
import Style from '../../../assets/styles/styles';
import Color from '../../../assets/styles/colors';
import ListBlock from '../../shared/ListBlock';
import { Text } from '@ui-kitten/components';
import TitleBlock from '../../shared/TitleBlock';
import moment from 'moment';
import _ from 'lodash';
import constants from '../../../_constants/constants';
import Loading from '../../../_helpers/Loading';
import { deleteUserDocument, getUserDocuments } from '../../../_services/documentsService';
import { Capitalize, GroupByMonth } from '../../../_helpers/helper';
import { SwipeRow } from 'react-native-swipe-list-view';
import * as Animatable from 'react-native-animatable';
import { ShowToast } from '../../../_actions/toast.action';
import { connect } from 'react-redux';
import * as WebBrowser from 'expo-web-browser';

const DocumentScreen = (props) => {
  const { clientId, refresh } = props;
  const [row, setRow] = useState({});
  const { ShowToast, navigation } = props;
  const [nonMediaDocuments, setNonMediaDocuments] = useState({});
  const [isLoading, setIsLoadingScreen] = useState(true);

  useEffect(() => {
    GetUserDocuments();
  }, [refresh]);

  const GetUserDocuments = async () => {
    getUserDocuments(false).then(res => {
      
      setNonMediaDocuments(GroupByMonth(res));
      setIsLoadingScreen(false);
    }).catch(ex => setIsLoadingScreen(false))
  }

  const deleteDocument = (item, i) => {
    deleteUserDocument(item.GeneralDocumentId, item.Id).then(res => {
      GetUserDocuments();

      ShowToast({ 'message': res.Message, 'type': res.Success ? 'success' : 'warning', "position": "bottom" });
      row[i].closeRow();
    }
    ).catch(err => {
      console.log(err);
    })
  }

  const DeleteMessageAlert = (item, i) => {
    Alert.alert(
      "Delete File",
      "Are you sure you want to delete this file?",
      [
        {
          text: "Cancel",
          style: 'destructive'
        },
        {
          text: "OK",
          onPress: () => {
            deleteDocument(item, i);


          },
          style: "cancel"
        }
      ],
      { cancelable: false }
    );
  }

  const _handleViewFileExpoBrowser = async (item) => {
    await WebBrowser.openBrowserAsync(encodeURI(constants.API_URL+`ViewFile?docId=${item.Id}&fileExtension=${item.FileExtension}&fileName=${item.FileName}&clientId=${clientId}`))
    .then(res=>{
    }).catch(err=>{
      console.log(err);
      
    });
  };
  // const _handleDownloadFileExpoBrowser = async (item) => {
  //   await WebBrowser.openBrowserAsync(encodeURI(constants.API_URL + `Download?docId=${item.Id}&fileExtension=${item.FileExtension}&fileName=${item.FileName}&clientId=${clientId}`));
  // };

  return (
    <SafeAreaView style={[Color.lighterBg, Style.mainContainer]}>
      <ScrollView style={[]}>
        {
          isLoading ? <Loading /> :
            _.isEmpty(nonMediaDocuments) ?
              <Animatable.View
                animation="fadeInUp"
                duration={300}
                delay={300}
                iterationCount={1}
                style={[Color.whiteBg, Style.containerPadding, { marginTop: 40 }]}
              >
                <ListBlock
                  title="No Documents Available"
                />
              </Animatable.View>
              :
              Object.keys(nonMediaDocuments).map((key, i) => {
                return (
                  <Animatable.View
                    animation={ Platform.OS === "android" ? null : "fadeInUp"}
                    duration={300}
                    delay={300}
                    iterationCount={1}
                    key={i}
                    style={[Color.whiteBg]}
                  >
                    <TitleBlock
                      key={i}
                      title={Capitalize(key)}
                      titleBlockStyle={[Color.lighterBg, { paddingTop: 15 }]}
                      titleBlockTextStyle={[Color.blackColor, { fontSize: 14, fontFamily: 'OpenSans-SemiBold' }]}
                    />
                    {
                      nonMediaDocuments[key].map((item, i) =>
                        <SwipeRow
                          key={i}
                          ref={ref => ref &&
                            setRow(row[i] = ref)}
                          disableRightSwipe={true}
                          rightOpenValue={-80}
                          stopRightSwipe={-100}
                        >
                          <TouchableOpacity
                            activeOpacity={0.5}
                            onPress={() => DeleteMessageAlert(item, i)}
                            style={[Style.swipeListActionContainer, Color.redBg]}>
                            <Text allowFontScaling={false} style={[Style.swipeListActionText, Color.whiteColor, {}]}>
                              Delete
                        </Text>
                          </TouchableOpacity>
                          <View style={[Color.whiteBg]}>
                            <ListBlock
                              key={i}
                              title={item.FileName}
                              subtitle={moment(item.UpdatedOn).format('ddd, MMM D, h:mm a')}
                              iconLeft="file-text"
                              iconLeftColor={Color.redColor}
                              iconRight="download"
                              iconRightColor={Color.blueColor}
                              // actionTitleOnPress={() => _handleViewFileExpoBrowser(item)}
                              // iconActionOnPress={() => _handleDownloadFileExpoBrowser(item)}
                              actionOnPress={() => _handleViewFileExpoBrowser(item)}
                              listBlockStyle={[Style.containerPadding]}
                            />
                          </View>
                        </SwipeRow>

                      )

                    }

                  </Animatable.View>
                )
              })}
      </ScrollView>
    </SafeAreaView >
  )
}

const mapDispatchToProps = {
  ShowToast
};
export default connect(null, mapDispatchToProps)(DocumentScreen);
