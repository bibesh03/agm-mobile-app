import React, { useState, useEffect, useRef } from 'react';
import { ScrollView } from 'react-native-gesture-handler';
import { SafeAreaView, View, Linking } from 'react-native';
import Style from '../../../assets/styles/styles';
import Color from '../../../assets/styles/colors';
import TitleBlock from '../../shared/TitleBlock';
import ListBlock from '../../shared/ListBlock';
import { getTasks } from '../../../_services/taskService';
import _ from 'lodash';
import Loading from '../../../_helpers/Loading';
import { GroupByMonth } from '../../../_helpers/helper';
import * as Animatable from 'react-native-animatable';

const CompletedTaskScreen = (props) => {
    const { navigation } = props;
    const [completedTask, setCompletedTask] = useState({});
    const [isLoading, setIsLoadingScreen] = useState(true);


    useEffect(() => {
        getTasksToUpdateState();
    }, []);

    useEffect(() => {
        const navigationUnPlug = navigation.addListener(
            'focus',
            () => {
                setIsLoadingScreen(true);
                getTasksToUpdateState();
            }
        );
        return navigationUnPlug;
    }, [navigation])

    const getTasksToUpdateState = async () => {
        await getTasks(true).then(res => {
            setIsLoadingScreen(false);
            setCompletedTask(GroupByMonth(res));
        }).catch(err => {
            setIsLoadingScreen(false)
        });
    }

    return (
        <SafeAreaView style={[Color.lighterBg, Style.mainContainer]}>
            <ScrollView style={[{}]}>
                {
                    isLoading ? <Loading /> :
                        _.isEmpty(completedTask) ?
                            <Animatable.View
                                animation="fadeInUp"
                                duration={300}
                                delay={300}
                                iterationCount={1}
                                style={[Color.whiteBg, Style.containerPadding, { marginTop: 40 }]}
                            >
                                <ListBlock
                                    title="No Completed Tasks Available"
                                />
                            </Animatable.View>
                            :
                            Object.keys(completedTask).map((key, i) => {
                                return (
                                    <Animatable.View
                                        animation="fadeInUp"
                                        duration={300}
                                        delay={300}
                                        iterationCount={1}
                                        key={i}
                                        style={[Color.whiteBg]}
                                    >

                                        <TitleBlock
                                            key={i}
                                            title={key}
                                            titleBlockStyle={[Color.lighterBg, { paddingTop: 15 }]}
                                            titleBlockTextStyle={[Color.blackColor, { fontSize: 14, fontFamily: 'OpenSans-SemiBold' }]}
                                        />
                                        {
                                            completedTask[key].map((item, i) => {
                                                return (
                                                    <ListBlock
                                                        key={i}
                                                        title={item.Name}
                                                        iconLeft="check-circle"
                                                        iconLeftColor={Color.lightGreenColor}
                                                        iconRight="chevron-right"
                                                        listBlockStyle={[Style.containerPadding]}
                                                        actionOnPress={() => { props.navigation.navigate('NonModalScreen', { screen: 'IndividualTask', params: { clientEmployee_CETaskId: item.ClientEmployee_CETaskId } }) }}
                                                    />
                                                )
                                            })
                                        }
                                    </Animatable.View>
                                )
                            })}
            </ScrollView>
        </SafeAreaView>
    )
}

export default CompletedTaskScreen;