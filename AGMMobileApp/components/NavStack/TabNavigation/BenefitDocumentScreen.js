import React, { useState, useEffect } from 'react';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { SafeAreaView, View, Linking, Image, StyleSheet } from 'react-native';
import Style from '../../../assets/styles/styles';
import Color from '../../../assets/styles/colors';
import ListBlock from '../../shared/ListBlock';
import TitleBlock from '../../shared/TitleBlock';
import moment from 'moment';
import _ from 'lodash';
import constants from '../../../_constants/constants';
import Loading from '../../../_helpers/Loading';
import { getBenefitsDocuments, getUserDocuments } from '../../../_services/documentsService';
import { Capitalize, GroupByFolder } from '../../../_helpers/helper';
import * as Animatable from 'react-native-animatable';
import * as WebBrowser from 'expo-web-browser';

const BenefitDocumentScreen = (props) => {
  const { clientId, navigation } = props;
  const [benefitDocuments, setBenefitDocuments] = useState({});
  const [result, setResult] = useState(null);
  const [isLoading, setIsLoadingScreen] = useState(true);

  useEffect(() => {
      getBenefitsDocuments().then(res => {
        console.log('res',res);
        
        setBenefitDocuments(GroupByFolder(res));
        setIsLoadingScreen(false);
      }).catch(ex => setIsLoadingScreen(false))
  }, []);

  const _handlePressButtonAsync = async (item) => {
    let result = await WebBrowser.openBrowserAsync(encodeURI(constants.API_URL+`Download?docId=${item.Id}&fileExtension=${item.FileExtension}&fileName=${item.FileName}&clientId=${clientId}`))
    .then(res=>{
    }).catch(err=>{
      console.log(err);
    });
  };

  return (
    <SafeAreaView style={[Color.lighterBg, Style.mainContainer]}>
      <ScrollView style={[]}>
        {
          isLoading ? <Loading /> :
            _.isEmpty(benefitDocuments) ?
              <Animatable.View
                animation="fadeInUp"
                duration={300}
                delay={300}
                iterationCount={1}
                style={[Color.whiteBg, Style.containerPadding, { marginTop: 40 }]}
              >
                <ListBlock
                  title="No Documents Available"
                />
              </Animatable.View>
              :
              Object.keys(benefitDocuments).map((key, i) => {
                return (
                  
                  <Animatable.View
                    animation="fadeInUp"
                    duration={300}
                    delay={300}
                    iterationCount={1}
                    key={i}
                    style={[Color.whiteBg]}
                  >
                    <TitleBlock
                      key={i}
                      title={Capitalize(key)}
                      titleBlockStyle={[Color.lighterBg, { paddingTop: 15 }]}
                      titleBlockTextStyle={[Color.blackColor, { fontSize: 14, fontFamily: 'OpenSans-SemiBold' }]}
                    />

                    {
                      benefitDocuments[key].map((item, i) => {
                        return (
                          <ListBlock
                            key={i}
                            title={item.FileName}
                            // subtitle={moment(item.UpdatedOn).format('MM-DD-YYYY - h:mm:ss a')}
                            iconLeft="file-text"
                            iconLeftColor={Color.redColor}
                            iconRight="download"
                            iconRightColor={Color.blueColor}
                            // actionTitleOnPress={() => _handlePressButtonAsync(item)}
                            // iconActionOnPress={() => Linking.openURL(constants.API_URL + `ViewFile?docId=${item.Id}&fileExtension=${item.FileExtension}&fileName=${item.FileName}&clientId=${clientId}`)}
                            actionOnPress={() =>_handlePressButtonAsync(item)}
                            listBlockStyle={[Style.containerPadding]}
                          />
                        )
                      })
                    }
                  </Animatable.View>
                )
              })}
      </ScrollView>
    </SafeAreaView >
  )
}

export default BenefitDocumentScreen;
