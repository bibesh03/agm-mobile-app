import React, { useState, useEffect } from 'react';
import { ScrollView } from 'react-native-gesture-handler';
import { SafeAreaView, View, Linking, Image } from 'react-native';
import Style from '../../../assets/styles/styles';
import Color from '../../../assets/styles/colors';
import ListBlock from '../../shared/ListBlock';
import TitleBlock from '../../shared/TitleBlock';
import * as Animatable from 'react-native-animatable';
import _ from 'lodash';
import * as WebBrowser from 'expo-web-browser';
import Loading from '../../../_helpers/Loading';
import { getLinks } from '../../../_services/documentsService';
import { GroupByMonth } from '../../../_helpers/helper';

const LinksScreen = (props) => {
  const [links, setLinks] = useState({});
  const [isLoading, setIsLoadingScreen] = useState(true);
  const [result, setResult] = useState(null);

  useEffect(() => {
    getLinks().then(res => {
      setLinks(GroupByMonth(res));
      setIsLoadingScreen(false);
    }).catch(ex => setIsLoadingScreen(false))
  }, []);

  const _handleLinkExpoBrowser = async (link) => {

    let result = await WebBrowser.openBrowserAsync(encodeURI(link)).then(res => {
    }).catch(err => {
      console.log(err);
    });
  };
  return (
    <SafeAreaView style={[Color.lighterBg, Style.mainContainer]}>
      <ScrollView style={[{ paddingTop: '10%' }]}>
        {
          isLoading ? <Loading /> :
            (_.isEmpty(links) ?
              <Animatable.View
                animation="fadeInUp"
                duration={300}
                delay={300}
                iterationCount={1}
                style={[Color.whiteBg, Style.containerPadding]}
              >
                <ListBlock
                  title="No Links Available"
                />
              </Animatable.View>
              :
              Object.keys(links).map((key, i) => {
                return (
                  <Animatable.View
                    key={i}
                    animation="fadeInUp"
                    duration={300}
                    delay={300}
                    iterationCount={1}
                    style={[Color.whiteBg]}
                  >
                    {
                      links[key].map((item, i) => {
                        return (
                          <ListBlock
                            key={i}
                            title={item.Title}
                            iconRight="chevron-right"
                            actionOnPress={() =>_handleLinkExpoBrowser(item.Link)}
                            titleStyle={[Color.blueColor]}
                            listBlockStyle={[Style.containerPadding]}
                          />
                        )
                      })
                    }
                  </Animatable.View>
                )
              }))}
      </ScrollView>
    </SafeAreaView>
  )
}

export default LinksScreen;

