import React, { useState, useEffect } from 'react';
import { StyleSheet, Text } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import Login from '../../authenticate/Login';
import Register from '../../authenticate/Register';
import AppStackScreen from './Modal/AppStackScreen';
import Style from '../../../assets/styles/styles';
import { Icon } from 'react-native-elements';
import { getToken } from '../../../_services/authenticateService';

const AuthStack = createStackNavigator();

const AuthStackScreen = (props) => {
  const [user, setUser] = useState(null);
  const [token, setToken] = useState(null);

  useEffect(() => {

    const findTokenAsync = async () => {
      try {
        props.route.params !== undefined ? setToken(props.route.params.token) : '';
        getToken().then(res => setUser(res));
      } catch (e) {
        //console.log("error");
      }
    }
    findTokenAsync();
  }, []);

  return (
    <AuthStack.Navigator
      initialRouteName={Login}
      mode="card"
      screenOptions={{
        headerShown: true,
        headerTitleAlign: 'center',
        headerRightContainerStyle: { padding: 20 }
      }}>
      <>
        {user == null && token == null ?
          (
            <>
              <AuthStack.Screen
                name="Login"
                component={Login}
                options={{
                  title: 'Login',
                  headerShown: false,
                  headerRight: null
                }}
              />
              <AuthStack.Screen
                name="Register"
                component={Register}
                options={({ navigation }) => ({
                  title: '',
                  headerLeft: (props) => (<Icon name='x' type='feather' size={25} style={[Style.iconHeader, {}]} onPress={() => navigation.goBack()}></Icon>),
                  headerLeftContainerStyle: { padding: 15, paddingLeft: Platform.OS === 'ios' ? 10 : 15 },
                })}
              />
            </>) : (
            <>
              <AuthStack.Screen
                name="AppStackScreen"
                children={(props) => <AppStackScreen {...props} />}
                options={({ navigation }) => ({
                  title: '',
                  headerLeftContainerStyle: { padding: 15, paddingLeft: Platform.OS === 'ios' ? 10 : 15 },
                  headerShown: false,
                  headerTitleAlign: 'center',
                })}
              />
            </>
          )
        }
      </>
    </AuthStack.Navigator>
  );
};

export default AuthStackScreen;