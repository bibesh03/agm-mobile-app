import React, { useState, useEffect } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { TouchableOpacity, Alert } from 'react-native';
import Style from '../../../../assets/styles/styles';
import Color from '../../../../assets/styles/colors';
import { Text } from '@ui-kitten/components';
import { Icon } from 'react-native-elements';

import Task from '../../../enTouch/Task';
import Settings from '../../../enTouch/Settings';
import Dashboard from '../../../dashboard/Dashboard';
import BenefitsCenter from '../../../enTouch/BenefitsCenter'
import IndividualTask from '../../../enTouch/IndividualTask';
import DocumentsCenter from '../../../enTouch/DocumentsCenter';
import MessageCenter from '../../../enTouch/MessageCenter';
import TabScreen from '../../../shared/TabScreen';
import AccountInfo from '../../../enTouch/AccountInfo';
import UpdateAccountInfo from '../../../enTouch/UpdateAccountInfo';
import ChangePassword from '../../../enTouch/ChangePassword';
import Messages from '../../../enTouch/Messages';
import ProviderInfo from '../../../enTouch/ProviderInfo';

import { logOut, logOutUser } from '../../../../_services/authenticateService';
import { ShowToast } from '../../../../_actions/toast.action';
import { connect } from 'react-redux';


const NonModalStack = createStackNavigator();

const NonModalStackScreen = (props) => {

  const { ShowToast } = props;
  const LogOut = () => {
    Alert.alert(
      "Sign Out",
      "Are you sure you want to log out of the app?",
      [
        {
          text: "Cancel",
          style: "destructive",
        },
        {
          text: "Yes",
          onPress: () => {
            logMeOut();
          },
          style: "cancel",
        },
      ],
      { cancelable: false }
    );
  };

  const logMeOut = () => {
    logOut();
    logOutUser().then(res => {
      props.navigation.reset({
        index: 0,
        routes: [{ name: 'AuthStackScreen' }]
      });
      // ShowToast({ 'message': 'Successfully Logged Out!', 'type': 'success' });
    });

  }
  return (
    <NonModalStack.Navigator
      initialRouteName={Dashboard}
      screenOptions={({ navigation }) => ({
        headerTitleAllowFontScaling: false,
        headerShown: true,
        headerRightContainerStyle: {
          padding: 15
        },
        headerTitleAlign: 'center',
        headerLeft: () => (<Icon name='chevron-left' type='feather' size={30} style={[Style.iconHeader]} onPress={() => navigation.goBack()}></Icon>),
        headerLeftContainerStyle: { padding: 15, paddingLeft: 5 },
      })}>

      <NonModalStack.Screen
        name="Dashboard"
        children={(props) => <Dashboard {...props} />}
      />

      <NonModalStack.Screen
        name="Settings"
        children={(props) => <Settings {...props} />}
        options={{
          title: 'Settings',
          headerRight: () =>
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={() => LogOut()}
              style={{ height: 25 }}
            >
              <Text
                allowFontScaling={false}
                style={[
                  Color.blackColor,
                  {
                    textTransform: "capitalize",
                    fontWeight: "400",
                    fontSize: 16,
                    marginRight: 5,
                  },
                ]}
              >
                Logout
              </Text>
            </TouchableOpacity>
        }}
      />

      <NonModalStack.Screen
        name="DocumentsCenter"
        component={DocumentsCenter}
        options={{
          title: 'My Documents'
        }}
      />
      <NonModalStack.Screen
        name="MessageCenter"
        component={MessageCenter}
        options={{
          title: 'Messages'
        }}
      />

      <NonModalStack.Screen
        name="BenefitsCenter"
        component={BenefitsCenter}
        options={({ navigation }) => ({
          title: 'My Benefits',
        })}
      />
      <NonModalStack.Screen
        name="TabScreen"
        component={TabScreen}
        options={({ navigation }) => ({
          title: 'UploadDocuments',
        })}
      />

      <NonModalStack.Screen
        name="Task"
        component={Task}
        options={({ navigation }) => ({
          title: 'My Tasks',
        })}
      />

      <NonModalStack.Screen
        name="IndividualTask"
        component={IndividualTask}
        options={({ navigation }) => ({
          title: 'Task Details',
        })}
      />

      <NonModalStack.Screen
        name="AccountInfo"
        component={AccountInfo}
        options={({ navigation }) => ({
          title: 'Account Information',
        })}
      />

      <NonModalStack.Screen
        name="UpdateAccountInfo"
        component={UpdateAccountInfo}
        options={({ navigation }) => ({
          title: 'Update Information',
        })}
      />

      <NonModalStack.Screen
        name="ChangePassword"
        component={ChangePassword}
        options={({ navigation }) => ({
          title: 'Change Password',
        })}
      />

      <NonModalStack.Screen
        name="Messages"
        component={Messages}
        options={({ navigation }) => ({
          title: '',
        })}
      />

      <NonModalStack.Screen
        name="ProviderInfo"
        component={ProviderInfo}
        options={({ navigation }) => ({
          title: '',
        })}
      />

    </NonModalStack.Navigator>
  )
};

const mapDispatchToProps = {
  ShowToast
};


export default connect(null, mapDispatchToProps)(NonModalStackScreen);