import React, { useState, useEffect } from 'react';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import NonModalStackScreen from '../non-Modal/NonModalStackScreen';
import NotificationCenter from '../../../enTouch/NotificationCenter';
import Style from '../../../../assets/styles/styles';
import { Icon } from 'react-native-elements';
import UploadDocModal from '../Modal/UploadDocModal';
import NewMessageModal from '../Modal/NewMessageModal';

const AppStack = createStackNavigator();

const AppStackScreen = (props) => {

  return (
    <AppStack.Navigator
      mode='modal'
      screenOptions={{
        headerTitleAllowFontScaling: false,
        headerShown: false,
        headerTitleAlign: 'center',
      }}>

      <AppStack.Screen
        name="NonModalScreen"
        children={(props) => <NonModalStackScreen {...props} />}
      />

      <AppStack.Screen
        name="NotificationCenter"
        component={NotificationCenter}
        options={({ navigation }) => ({
          title: 'Notifications',
          headerShown: true,
          headerLeft: (props) => (<Icon name='x' type='feather' size={25} style={[Style.iconHeader, {}]} onPress={() => navigation.goBack()}></Icon>),
          headerLeftContainerStyle: { padding: 15, paddingLeft: Platform.OS === 'ios' ? 10 : 15 },
        })}
      />

      <AppStack.Screen
        name="UploadDocModal"
        component={UploadDocModal}
        options={({ navigation }) => ({
          title: 'UploadDocuments',
          headerShown: true,
          headerLeft: (props) => (<Icon name='x' type='feather' size={25} style={[Style.iconHeader, {}]} onPress={() => navigation.goBack()}></Icon>),
          headerLeftContainerStyle: { padding: 15, paddingLeft: Platform.OS === 'ios' ? 10 : 15 },
        })}
      />

      <AppStack.Screen
        name="NewMessageModal"
        component={NewMessageModal}
        options={({ navigation }) => ({
          gestureEnabled: true,
          cardOverlayEnabled: true,
          ...TransitionPresets.ModalPresentationIOS,
          title: '',
        })}
      />


    </AppStack.Navigator>
  );
};

export default AppStackScreen;