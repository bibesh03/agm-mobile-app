import React, { useEffect, useState, useRef } from 'react';
import { SafeAreaView, ScrollView, View, KeyboardAvoidingView, Platform, TouchableOpacity, Keyboard } from 'react-native';
import { Text } from '@ui-kitten/components';
import { Icon, Input } from 'react-native-elements';
import Style from '../../../../assets/styles/styles';
import Color from '../../../../assets/styles/colors';
import * as Animatable from 'react-native-animatable';
import { getToken, GetClientID } from '../../../../_services/authenticateService';
import ModalHeader from '../../../shared/ModalHeader';
import { sendSupportMessage } from '../../../../_services/supportService';
import Loading from '../../../../_helpers/Loading';

const NewMessage = (props) => {
    const { navigation } = props;
    const [textMessage, setTextMessage] = useState('');
    const [user, setUser] = useState({});
    const [sendButtonColor, setSendButtonColor] = useState(Color.lightGreyBg);
    const [supportRequest, setSupportRequest] = useState({ UserId: '', EmailId: '', Username: '', Subject: '', Message: '', MessageId: 0, clientId: '' });
    const [subject, setSubject] = useState('');
    const [isloading, setIsLoading] = useState(false);

    const scrollViewRef = useRef();

    useEffect(() => {
        getUser();
        GetClientID().then(res => {
            setSupportRequest({ ...supportRequest, clientId: res })
        })
    }, []);

    useEffect(() => {
        const navigationUnPlug = navigation.addListener(
            'focus',
            () => {
                getUser();
            }
        );
        return navigationUnPlug;
    }, [navigation]);

    const getUser = async () => {
        await getToken().then(res => {
            setUser(res);
        }).catch(err => {
            console.log(err);
        })
    }

    const sendSupportRequest = async () => {
        await sendSupportMessage({
            ...supportRequest,
            UserId: user.UserID,
            Message: textMessage,
            Subject: subject,
            EmailId: user.Email,
            Username: user.Username,
            MessageId: 0,
        }).then(res => {
            props.navigation.navigate('MessageCenter');
        }).catch(err => console.log(err))
    }

    const ScrollToBottom = () => {
        setTimeout(() => {
            scrollViewRef.current.scrollToEnd({ animated: true });
        }, 200)
    };

    const SetMessageText = (val) => {
        setTextMessage(val);
        setSendButtonColor((val !== '') ? Color.blueBg : Color.lightGreyBg);
    }

    const createSubject = (val) => {
        setSubject(val);
    }

    const RemoveFocus = () => {
        Keyboard.dismiss();
    }

    return (
        <KeyboardAvoidingView
            behavior={Platform.OS === "ios" ? "padding" : "height"}
            keyboardVerticalOffset={40}
            style={[Color.lighterBg, Style.mainContainer, { paddingTop: '5%' }]}
        >
            <ModalHeader
                title="New Message"
                textRight="Cancel"
                titleBlockContainerStyle={{ borderBottomWidth: 0 }}
                actionOnPressRight={() => props.navigation.goBack()}
            />
            {
                isloading ? <Loading /> :
                    <>

                        <View>
                            <Input
                                autoFocus
                                allowFontScaling={false}
                                inputStyle={[Style.inputBorderedStyle]}
                                inputContainerStyle={[Style.inputBorderedContainer, Color.whiteBg, { borderRadius: 0, minHeight: 40 }]}
                                containerStyle={[Style.inputBorderedMainContainer, {}]}
                                clearButtonMode="while-editing"
                                leftIcon={(
                                    <Text allowFontScaling={false} style={[Color.darkGreyColor, { opacity: 0.8 }]}>Subject: </Text>
                                )}
                                onChangeText={(val) => createSubject(val)}
                            />
                        </View>
                        <ScrollView
                            ref={scrollViewRef}
                            onContentSizeChange={() => ScrollToBottom()}
                            style={[Color.lighterBg, { padding: 15 }]}
                        >

                        </ScrollView>

                        <View style={[Style.messageMainInputContainer, Color.lighterBg]}>
                            <View style={[Style.messageInputContainer]}>
                                <Input
                                    allowFontScaling={false}
                                    placeholder="New Message"
                                    placeholderTextColor="#B1B8CA"
                                    inputStyle={[Style.inputBorderedStyle]}
                                    inputContainerStyle={[Style.inputBorderedContainer, Color.whiteBg]}
                                    containerStyle={[Style.inputBorderedMainContainer]}
                                    onBlur={() => RemoveFocus()}
                                    onChangeText={(val) => SetMessageText(val)}
                                />
                            </View>
                            <View style={[Style.messageSendButtonContainer]}>

                                <TouchableOpacity
                                    activeOpacity={0.5}
                                    disabled={(textMessage == '' || subject == '')}
                                    onPress={() => {
                                        setIsLoading(true);
                                        sendSupportRequest();
                                    }}
                                    style={[Style.messageSendIcon, sendButtonColor]}>
                                    <Icon
                                        name="send"
                                        type="material-community"
                                        size={25}
                                        iconStyle={[Color.whiteColor, { margin: 0, paddingLeft: 3 }]}
                                    />
                                </TouchableOpacity>
                            </View>

                        </View>
                    </>
            }
        </KeyboardAvoidingView>
    );
}

export default NewMessage;