import React, { useState, useEffect } from 'react';
import { Card, Icon } from 'react-native-elements'
import { SafeAreaView, TouchableOpacity, View, Alert, StyleSheet, Dimensions, Image } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import * as DocumentPicker from 'expo-document-picker';
import { Text, Input, Button, Spinner } from '@ui-kitten/components';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import Constants from 'expo-constants';
import * as FileSystem from 'expo-file-system';
import * as IntentLauncher from 'expo-intent-launcher';
import { uploadDocuments } from '../../../../_services/documentsService';
import { getToken } from '../../../../_services/authenticateService';
import CardSmall from '../../../shared/CardSmall';
import Style from '../../../../assets/styles/styles';
import Color from '../../../../assets/styles/colors';
import { FileSystemUploadType } from 'expo-file-system';
import PDFReader from 'rn-pdf-reader-js';
import FontStyle from '../../../../assets/styles/font';

const UploadDocModal = (props) => {
  const [singleFile, setSingleFile] = useState({ file: null });
  const [image, setImage] = useState({ image: null });
  const [doc, setDoc] = useState('');
  const [fileName, setFileName] = useState([]);
  const { clientID, genDocID, docInfoID } = props.route.params;
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [flag, setFlag] = useState(false); //sets the flag if user takes picture(true) or uploads from directory(false)


  useEffect(() => {
    setDoc(null);
    setFileName([]);
    setIsSubmitting(false);
    setFlag(false);
  }, []);

  //saves the document either uploaded from directory or taken picture by the camera
  //FileName is introduced to set fileName of pictures taken by the phone so that users can take picture and set their fileNames.
  const save = (clientID) => {
    let formData = new FormData();


    if (fileName.length == 1) {
      fileName[1] = doc.uri.split('/').pop().split('#')[0].split('?')[0].split('.')[1];
    }

    ((fileName[1] !== 'pdf') ?
      formData.append('files', {
        uri: doc.uri,
        type: `image/${fileName[1]}`,
        name: `${fileName[0]}.${fileName[1]}`
      })
      :
      (
        formData.append('files', {
          uri: doc.uri,
          type: `application/${fileName[1]}`,
          name: `${fileName[0]}.${fileName[1]}`
        })
      ))
    // docId = 0 in uploadDocuments means that it is the new document uploaded
    // we send in specific document id in case of replace functionality
    uploadDocuments(clientID, docInfoID, genDocID, formData).then(res => {
      setIsSubmitting(false);
      setDoc(null);
      setFileName([]);
      props.navigation.goBack();
    }).catch(err => console.log(err)
    )
  }

  //uploads the document of any type like pdf or any kinds of images
  const Doc = async () => {
    await DocumentPicker.getDocumentAsync({
      type: '*/*',
    }).then(res => {
      if (res.type == 'success') {
        setDoc(res);
        setFileName([res.name.split('.')[0], res.name.split('.')[1]]);
        setFlag(false);
      };
    }).catch(err => {
      console.log('error while document picking', err)
    });

  };

  //takes picture to upload
  const Camera = async () => {
    try {
      const result = await ImagePicker.launchCameraAsync({
        allowsEditing: true,
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        quality: 1,
      });
      if (!result.cancelled) {
        setDoc(result);
        setFlag(true);
      }
    }
    catch (e) {
      // console.log(e);
    }
  }

  // LOADING INDICATOR
  const loadingIndicator = (props) => (
    <View style={[props.style]}>
      <Spinner status='basic' size='small' />
    </View>
  );


  const getPermissionAsync = async () => {
    if (Constants.platform.ios || Constants.platform.android) {
      const permission = await Permissions.askAsync(Permissions.CAMERA);
      if (permission.status !== 'granted') {
        return (<Text>Access to camera has been denied</Text>)
      }
      Camera();
    }
  }

  return (

    <SafeAreaView style={[Color.lighterBg, Style.mainContainer]}>
      <ScrollView style={[Style.containerPadding, { paddingTop: '5%' }]}>
        <View style={{ flex: 1 }}>
          <View style={styles.activeImageContainer}>
            {doc ? (
              (fileName[1]) !== 'pdf' ? (
                <>
                  <Image source={{ uri: doc.uri }} style={{ flex: 1 }} />
                </>
              ) : (
                  <>
                    <PDFReader
                      source={{ uri: doc.uri }}
                      withPinchZoom
                    />
                  </>
                )
            ) : (
                <View />
              )}
          </View>
          {(doc === null) ? (
            <View style={{ marginBottom: '10%' }}>

              <View style={{ marginTop: 20 }}>
                <TouchableOpacity>
                  <Button
                    onPress={() => Doc()}
                    style={[Style.roundButton]}
                  >
                    <Text
                      allowFontScaling={false}
                      style={[FontStyle.formButton, {}]} >Upload</Text>
                  </Button>
                </TouchableOpacity>
              </View>

              <View style={{ marginTop: 20 }}>
                <TouchableOpacity>
                  <Button
                    onPress={() => getPermissionAsync()}
                    style={[Style.roundButton]}
                  >
                    <Text
                      allowFontScaling={false}
                      style={[FontStyle.formButton, {}]} >Take Picture</Text>
                  </Button>
                </TouchableOpacity>
              </View>

              <View style={{ marginTop: 20 }}>
                <TouchableOpacity>
                  <Button
                    style={[Style.roundButton]}
                    onPress={() => {
                      setIsSubmitting(true);
                      save(clientID);
                    }}
                    disabled={isSubmitting || fileName.length == 0}
                    accessoryRight={isSubmitting ? loadingIndicator : ''} >
                    <Text
                      allowFontScaling={false}
                      style={[FontStyle.formButton, {}]} >Save</Text>
                  </Button>
                </TouchableOpacity>
              </View>

            </View>
          ) : (
              <View style={{ marginBottom: '10%' }}>
                {flag ?
                  <View style={{ marginTop: 20 }}>

                    <Input
                      placeholder='Name of your file'
                      value={fileName[0]}
                      onChangeText={nextValue => setFileName([nextValue])}
                    />
                  </View>
                  : <View />}


                <View style={{ marginTop: 20 }}>
                  {flag ?
                    <TouchableOpacity>
                      <Button
                        onPress={() => getPermissionAsync()}
                        style={[Style.roundButton]}
                      >
                        <Text
                          allowFontScaling={false}
                          style={[FontStyle.formButton, {}]} >Replace</Text>
                      </Button>
                    </TouchableOpacity> :
                    <TouchableOpacity>
                      <Button
                        onPress={() => Doc()}
                        style={[Style.roundButton]}
                      >
                        <Text
                          allowFontScaling={false}
                          style={[FontStyle.formButton, {}]} >Replace</Text>
                      </Button>
                    </TouchableOpacity>

                  }
                </View>

                <View style={{ marginTop: 20 }}>
                  <TouchableOpacity>
                    <Button
                      style={[Style.roundButton]}
                      onPress={() => {
                        setDoc(null);
                        setFileName([]);
                        setFlag(false);
                        setIsSubmitting(false);
                      }}
                    >
                      <Text
                        allowFontScaling={false}
                        style={[FontStyle.formButton, {}]} >Remove</Text>
                    </Button>
                  </TouchableOpacity>
                </View>

                <View style={{ marginTop: 20 }}>
                  <TouchableOpacity>
                    <Button
                      style={[Style.roundButton]}
                      onPress={() => {
                        setIsSubmitting(true);
                        save(clientID);
                      }}
                      disabled={isSubmitting || fileName.length == 0}
                      accessoryRight={isSubmitting ? loadingIndicator : ''} >
                      <Text
                        allowFontScaling={false}
                        style={[FontStyle.formButton, {}]} >Save</Text>
                    </Button>
                  </TouchableOpacity>
                </View>
              </View>
            )}
        </View>

      </ScrollView>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  activeImageContainer: {
    flex: 1,
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height / 2,
    backgroundColor: "#eee",
    borderBottomWidth: 0.5,
    borderColor: "#fff"
  },
});


export default UploadDocModal;