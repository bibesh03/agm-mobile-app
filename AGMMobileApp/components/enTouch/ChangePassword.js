import React, { useState, useEffect } from 'react';
import { SafeAreaView, View, TouchableWithoutFeedback, Keyboard } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Text, Input, Icon, Spinner } from '@ui-kitten/components';
import { connect } from 'react-redux';
import Button from '../shared/Button';

import Style from '../../assets/styles/styles';
import Color from '../../assets/styles/colors';
import FontStyle from '../../assets/styles/font';
import { Formik } from 'formik';

import ChangePasswordValidator from '../../_helpers/ChangePasswordValidator';
import { ShowToast } from '../../_actions/toast.action';
import { updateUserPasswod } from '../../_services/authenticateService';

const ChangePassword = (props) => {
    const { ShowToast } = props;
    const [secureTextEntryPassword, setSecureTextEntryPassword] = useState(true);
    const [secureTextEntryNewPassword, setSecureTextEntryNewPassword] = useState(true);
    const [secureTextEntryConfirmPassword, setSecureTextEntryConfirmPassword] = useState(true);
    const [changePwdModel, setChangePwdModel] = useState(
        {
            UserId: '',
            Username: '',
            CurrentPassword: '',
            NewPassword: '',
            ConfirmPassword: ''
        }
    );

    useEffect(() => {

    }, []);

    const ChangeUserPassword = (values, setSubmitting) => {
        Keyboard.dismiss();
        AsyncStorage.getItem('user').then(res => {
            let savedUser = JSON.parse(res);
            let newUserModel = { ...values };

            updateUserPasswod({ ...newUserModel, UserId: savedUser.UserID, Username: savedUser.Username }).then(res => {
                if (res.Success) {
                    ShowToast({ 'message': res.Message, 'type': 'success', "position": "top" });
                    props.navigation.navigate("Dashboard");
                } else {
                    ShowToast({ 'message': "Something went wrong! Please try correcting your current password.", 'type': 'danger', "position": "top" });
                    setSubmitting(false);
                }

            })
        });
    }

    const toggleSecureEntryPassword = () => {
        setSecureTextEntryPassword(!secureTextEntryPassword);
    }

    const toggleSecureEntryNewPassword = () => {
        setSecureTextEntryNewPassword(!secureTextEntryNewPassword);
    }

    const toggleSecureEntryConfirmPassword = () => {
        setSecureTextEntryConfirmPassword(!secureTextEntryConfirmPassword);
    }

    const showPassword = (props) => (
        <TouchableWithoutFeedback onPress={toggleSecureEntryPassword}>
            <Icon {...props} name={secureTextEntryPassword ? 'eye-off-outline' : 'eye-outline'} />
        </TouchableWithoutFeedback>
    );

    const showNewPassword = (props) => (
        <TouchableWithoutFeedback onPress={toggleSecureEntryNewPassword}>
            <Icon {...props} name={secureTextEntryNewPassword ? 'eye-off-outline' : 'eye-outline'} />
        </TouchableWithoutFeedback>
    );

    const showConfirmPassword = (props) => (
        <TouchableWithoutFeedback onPress={toggleSecureEntryConfirmPassword}>
            <Icon {...props} name={secureTextEntryConfirmPassword ? 'eye-off-outline' : 'eye-outline'} />
        </TouchableWithoutFeedback>
    );

    const setInputStatus = (touched, hasError) => {
        if (hasError) {
            return 'danger';
        } else if (touched && !hasError) {
            return 'success';
        } else {
            return ''
        }
    }

    return (
        <SafeAreaView style={[Color.lighterBg, Style.mainContainer]}>
            <Formik
                initialValues={changePwdModel}
                validationSchema={ChangePasswordValidator}
                onSubmit={(values, { setSubmitting }) => {
                    setSubmitting(true);
                    ChangeUserPassword(values, setSubmitting);
                }}
            >
                {({ handleChange, handleBlur, handleSubmit, values, errors, touched, isValid, setFieldTouched, isSubmitting, setFieldValue }) => (
                    <View style={[Style.containerPadding, { paddingTop: '7%' }]}>
                        <Text allowFontScaling={false} style={[FontStyle.formLabel]} category='label'>Enter Current Password</Text>
                        <Input
                            allowFontScaling={false}
                            style={[Style.formInput]}
                            textStyle={[FontStyle.formInput]}
                            size='large'
                            onChangeText={handleChange('CurrentPassword')}
                            onBlur={() => {
                                setFieldTouched('CurrentPassword');
                            }}
                            status={setInputStatus(touched.CurrentPassword, errors.CurrentPassword)}
                            value={values.CurrentPassword}
                            accessoryRight={showPassword}
                            secureTextEntry={secureTextEntryPassword}
                        />
                        {touched.CurrentPassword && errors.CurrentPassword &&
                            <Text allowFontScaling={false} style={[Style.errorMessageStyle]}>{errors.CurrentPassword}</Text>
                        }

                        <Text allowFontScaling={false} style={[FontStyle.formLabel]} category='label'>Enter New Password</Text>
                        <Input
                            allowFontScaling={false}
                            style={[Style.formInput]}
                            textStyle={[FontStyle.formInput]}
                            size='large'
                            onChangeText={handleChange('NewPassword')}
                            onBlur={() => {
                                setFieldTouched('NewPassword');
                            }}
                            status={setInputStatus(touched.NewPassword, errors.NewPassword)}
                            value={values.NewPassword}
                            accessoryRight={showNewPassword}
                            secureTextEntry={secureTextEntryNewPassword}
                        />
                        {touched.NewPassword && errors.NewPassword &&
                            <Text allowFontScaling={false} style={[Style.errorMessageStyle]}>{errors.NewPassword}</Text>
                        }

                        <Text allowFontScaling={false} style={[FontStyle.formLabel]} category='label'>Confirm New Password</Text>
                        <Input
                            allowFontScaling={false}
                            style={[Style.formInput]}
                            textStyle={[FontStyle.formInput]}
                            size='large'
                            onChangeText={handleChange('ConfirmPassword')}
                            onBlur={() => {
                                setFieldTouched('ConfirmPassword');
                            }}
                            status={setInputStatus(touched.ConfirmPassword, errors.ConfirmPassword)}
                            value={values.ConfirmPassword}
                            accessoryRight={showConfirmPassword}
                            secureTextEntry={secureTextEntryConfirmPassword}
                        />
                        {touched.ConfirmPassword && errors.ConfirmPassword &&
                            <Text allowFontScaling={false} style={[Style.errorMessageStyle]}>{errors.ConfirmPassword}</Text>
                        }

                        <View>
                            <Button
                                buttonText="Save New Password"
                                buttonColor={Color.lightGreenBg}
                                actionOnPress={() => handleSubmit()}
                                spinnerActive={isSubmitting}
                                activeOpacity={isSubmitting ? 1 : 0.5}
                            />
                        </View>
                    </View>
                )}
            </Formik>
        </SafeAreaView>
    );
}
const mapDispatchToProps = {
    ShowToast
};

export default connect(null, mapDispatchToProps)(ChangePassword);