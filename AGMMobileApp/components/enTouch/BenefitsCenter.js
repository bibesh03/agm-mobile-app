import React, { useState, useEffect, useRef } from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { getToken } from '../../_services/authenticateService';
import BenefitDocumentScreen from '../NavStack/TabNavigation/BenefitDocumentScreen'
import ProviderScreen from '../NavStack/TabNavigation/ProviderScreen';
import LinksScreen from '../NavStack/TabNavigation/LinksScreen';
import { Icon } from 'react-native-elements';
import Color from '../../assets/styles/colors'


const Tab = createBottomTabNavigator();

const BenefitsCenter = (props) => {
    const { navigation } = props;
    const [user, setUser] = useState({});
    const [clientId, setClientId] = useState(0);

    useEffect(() => {
        GetUser();
    }, []);

    useEffect(() => {
        const unsubscribe = navigation.addListener(
            'focus',
            () => {
                GetUser();
            }
        );
        return unsubscribe;
    }, [navigation]);

    const GetUser = async () => {
        await getToken().then(res => {
                setUser(res);
                setClientId(res.ClientID);
        }).catch(err => {
            console.log('error is', err);
        })
    }

    return (
        <Tab.Navigator
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused }) => {
                    if (route.name === 'DocumentView') {
                        return (
                            <Icon name='file-text' type='feather' size={20} color={focused ? Color.redColor.color : Color.midGreyColor.color} />
                        );
                    } else if (route.name === 'MediaView') {
                        return (
                            <Icon name='users' type='feather' size={20} color={focused ? Color.lightGreenColor.color : Color.midGreyColor.color} />
                        );
                    } else {
                        return (
                            <Icon name='link' type='feather' size={20} color={focused ? Color.blueColor.color : Color.midGreyColor.color} />
                        );
                    }
                }
            })}

            tabBarOptions={{
                allowFontScaling: false,
                // activeTintColor: '#42765E',
                activeTintColor: '#546062',
                inactiveTintColor: '#C0C3C4',
                labelPosition: 'below-icon',
                labelStyle: { fontSize: 12, fontWeight: '500' },
            }}
        >
            <Tab.Screen
                name='DocumentView'
                children={() => <BenefitDocumentScreen clientId={clientId} {...props} />}
                options={{
                    tabBarLabel: 'Documents',
                }}
            />
            <Tab.Screen
                name='LinksView'
                component={LinksScreen}
                options={{
                    tabBarLabel: 'Links',
                }}
            />
            <Tab.Screen
                name='MediaView'
                children={() => <ProviderScreen clientId={clientId} {...props} />}
                options={{
                    tabBarLabel: 'Providers',
                }}
            />


        </Tab.Navigator>
    )
}
export default BenefitsCenter;
