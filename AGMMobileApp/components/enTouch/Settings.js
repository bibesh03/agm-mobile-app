import React, { useState, useEffect } from 'react';
import { ScrollView } from 'react-native-gesture-handler';
import { SafeAreaView, View } from 'react-native';
import Color from '../../assets/styles/colors';
import Style from '../../assets/styles/styles';
import ListBlock from '../shared/ListBlock';
import { getToken } from '../../_services/authenticateService';

const Settings = (props) => {
    const [user, setUser] = useState({});

    useEffect(() => {
        getUser();
    }, []);

    useEffect(() => {
        const unsubscribe = props.navigation.addListener('focus', () => {
            getUser();
        });
        return unsubscribe;
    }, [props.navigation]);

    const getUser = async () => {
        await getToken().then(res =>
            setUser(res))
            .catch(err => console.log(err));
    }

    return (
        <SafeAreaView style={[Color.lighterBg, Style.mainContainer]}>
            <ScrollView style={{ paddingTop: '7%' }}>
                <View style={[Color.whiteBg]}>
                    <ListBlock
                        title="Manage Account"
                        listBlockStyle={[Style.containerPadding]}
                        iconRight="chevron-right"
                        actionOnPress={() => props.navigation.navigate('AccountInfo', { user: user })}
                    />

                    <ListBlock
                        title="Change Password"
                        listBlockStyle={[Style.containerPadding]}
                        iconRight="chevron-right"
                        actionOnPress={() => props.navigation.navigate('ChangePassword')}
                    />
                </View>
            </ScrollView>
        </SafeAreaView >
    )
}
export default Settings;
