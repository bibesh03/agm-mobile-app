import React, { useState, useEffect } from 'react';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { SafeAreaView, View } from 'react-native';
import { Text } from '@ui-kitten/components';
import Style from '../../assets/styles/styles';
import Color from '../../assets/styles/colors';
import TitleBlock from '../shared/TitleBlock';
import Button from '../shared/Button';
import { getIndividualTask } from '../../_services/taskService';
import { checkTask } from '../../_services/taskService';
import { ShowToast } from '../../_actions/toast.action';
import { connect } from 'react-redux';
import Loading from '../../_helpers/Loading';

const IndividualTask = (props) => {
  const { ShowToast, navigation } = props;
  const { clientEmployee_CETaskId } = props.route.params;
  const [individualTask, setIndividualTask] = useState({});
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [checked, setChecked] = useState(false);
  const [isLoading, setIsLoadingScreen] = useState(true);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      getTasksToUpdateState();
    });
    return unsubscribe;
  }, [navigation]);

  // props.navigation.setOptions({
  //   title: individualTask.Name,
  // });

  const getTasksToUpdateState = async () => {
    await getIndividualTask(clientEmployee_CETaskId).then(res => {
      setIsLoadingScreen(false);
      setIndividualTask(res);
      setChecked(res.IsChecked);
    }).catch(err => {
      //error getting the task
    })
  }

  const updateTaskCompletion = async () => {
    individualTask.IsChecked = !individualTask.IsChecked;
    checkTask(individualTask)
      .then(res => {
        UpdateTaskState();
        ShowToast({ 'message': res.Message, 'type': res.Success ? 'success' : 'warning', "position": "bottom" });
        setIsSubmitting(false)
      })
      .catch(err => console.log(err))
  }

  const UpdateTaskState = () => {
    setChecked(!checked);
  }

  return (

    <SafeAreaView style={[Color.whiteBg, Style.mainContainer]}>
      {
        isLoading ? <Loading /> :
          <TitleBlock

            title={individualTask.Name}
            iconRight={checked ? "check-circle" : "info"}
            iconRightColor={checked ? Color.lightGreenColor : Color.orangeColor}
            titleBlockStyle={[Color.lighterBg, { paddingTop: 15 }]}
            titleBlockTextStyle={[Color.blackColor, { fontSize: 18, fontFamily: 'OpenSans-SemiBold' }]}
          />
      }
      <ScrollView style={[]}>

        <View style={[Color.whiteBg]}>

          <View style={[Style.containerPadding, Color.whiteBg, { paddingVertical: 20, minHeight: '50%' }]}>

            <Text allowFontScaling={false}>

              {individualTask.Description}
            </Text>

          </View>
        </View>
      </ScrollView>

      <View style={[Style.containerPadding, { marginBottom: 20 }]}>

        {!isLoading ?
          individualTask.IsChecked ?
            (


              <Button
                buttonText="Mark as Incomplete"
                buttonColor={Color.orangeBg}
                disabled={isSubmitting}
                activeOpacity={isSubmitting ? 1 : 0.5}
                spinnerActive={isSubmitting}
                actionOnPress={
                  isSubmitting ?
                    () => { } :
                    () => {
                      setIsSubmitting(true);
                      updateTaskCompletion()
                    }
                }
              />)
            :
            (<Button
              buttonText="Mark as Complete"
              buttonColor={Color.lightGreenBg}
              disabled={isSubmitting}
              activeOpacity={isSubmitting ? 1 : 0.5}
              spinnerActive={isSubmitting}
              actionOnPress={
                isSubmitting ?
                  () => { } :
                  () => {
                    setIsSubmitting(true);
                    updateTaskCompletion()
                  }
              }
            />) : <></>
        }

      </View>

    </SafeAreaView>
  )
}

const mapDispatchToProps = {
  ShowToast
};

export default connect(null, mapDispatchToProps)(IndividualTask);