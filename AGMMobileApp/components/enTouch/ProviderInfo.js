import React, { useEffect, useState, useRef } from 'react';
import { SafeAreaView, ScrollView, View, Text, Linking } from 'react-native';
import ListBlock from '../shared/ListBlock';
import Style from '../../assets/styles/styles';
import Color from '../../assets/styles/colors';
import Card from '../../assets/styles/card';
import * as Animatable from 'react-native-animatable';
import { getToken } from '../../_services/authenticateService'
import * as WebBrowser from 'expo-web-browser';
import { capitalize } from 'lodash';
import { Capitalize } from '../../_helpers/helper';
import { Title } from 'react-native-paper';

const ProviderInfo = (props) => {
    const {providerInfo, title} = props.route.params;
    const [user, setUser] = useState({});

    useEffect(() => {
        getUser();
        props.navigation.setOptions({
            title: Capitalize(title),
        });
    }, []);

    const getUser = async () => {
        await getToken().then(res => {
            setUser(res);
            //   console.log(res);
        }).catch(err => console.log(err));
    }

    const OpenOnWebBrowser = async (link) => {
        await WebBrowser.openBrowserAsync(encodeURI(link)).then(res=>{

        }).catch(err => console.log(err));
    };

    return (
        <SafeAreaView style={[Color.lighterBg, Style.mainContainer]}>
            <ScrollView style={[Style.containerPadding, { paddingTop: '7%' }]}>
                <Animatable.View
                    animation="fadeInUp"
                    duration={300}
                    delay={300}
                    iterationCount={1}
                    style={[Card.base, Color.whiteBg]}
                >
                    <View>
                        <ListBlock
                            title="Provider Name"
                            subtitle={providerInfo.ProviderName}
                            fullWidthTitle
                            fullWidthSubtitle
                            titleStyle={[Color.greenColor, Style.listBlockTitleAlt]}
                            subtitleStyle={[Style.listBlockSubtitleAlt]}
                            listBlockStyle={[Style.containerPadding, { paddingTop: 0 }]}
                        />

                        <ListBlock
                            title="Support Email"
                            linkText={providerInfo.Email.toLowerCase()}
                            fullWidthTitle
                            fullWidthSubtitle
                            titleStyle={[Color.greenColor, Style.listBlockTitleAlt]}
                            linkTextStyle={{ fontSize: 18 }}
                            listBlockStyle={[Style.containerPadding]}
                            actionLinkOnPress={() =>providerInfo.Email != null?  Linking.openURL(`${'mailto'}:${providerInfo.Email}`) : ''}
                        />

                        <ListBlock
                            title="Primary Phone Number"
                            linkText={providerInfo.Phone}
                            fullWidthTitle
                            fullWidthSubtitle
                            titleStyle={[Color.greenColor, Style.listBlockTitleAlt]}
                            linkTextStyle={{ fontSize: 18 }}
                            listBlockStyle={[Style.containerPadding]}
                            actionLinkOnPress={() =>providerInfo.Phone != null?  Linking.openURL(`${'tel'}:${providerInfo.Phone}`) : ''}
                        />    
                        <ListBlock
                            title="Secondary Phone Number"
                            linkText={providerInfo.Sec_Phone != null ? providerInfo.Sec_Phone : "N/A" }
                            fullWidthTitle
                            fullWidthSubtitle
                            titleStyle={[Color.greenColor, Style.listBlockTitleAlt]}
                            linkTextStyle={{ fontSize: 18 }}
                            listBlockStyle={[Style.containerPadding]}
                            actionLinkOnPress={() =>providerInfo.Sec_Phone != null?  Linking.openURL(`${'tel'}:${providerInfo.Sec_Phone}`) : ''}
                    />

                        <ListBlock
                            title="Support Page / Website"
                            linkText={providerInfo.Website}
                            fullWidthTitle
                            fullWidthSubtitle
                            titleStyle={[Color.greenColor, Style.listBlockTitleAlt]}
                            linkTextStyle={{ fontSize: 16 }}
                            listBlockStyle={[Style.containerPadding, Style.listBlockFooter]}
                            actionLinkOnPress={() =>providerInfo.Website != null?   OpenOnWebBrowser(providerInfo.Website) : ''}
                            actionLinkOnPress={() => OpenOnWebBrowser(providerInfo.Website)}
                        />
                    </View>
                </Animatable.View>
            </ScrollView>
        </SafeAreaView >
    );
}

export default ProviderInfo;