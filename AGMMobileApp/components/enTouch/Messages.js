import React, { useEffect, useState, useRef } from 'react';
import { SafeAreaView, ScrollView, View, KeyboardAvoidingView, Platform, TouchableOpacity, RefreshControl, StyleSheet } from 'react-native';
import { Text } from '@ui-kitten/components';
import { Icon, Input } from 'react-native-elements';
import Style from '../../assets/styles/styles';
import Color from '../../assets/styles/colors';
import * as Animatable from 'react-native-animatable';
import { getToken } from '../../_services/authenticateService';
import ListBlock from '../shared/ListBlock';
import _ from 'lodash';
import { getConversationMessage, postIsRead, sendSupportMessage } from '../../_services/supportService';
import Loading from '../../_helpers/Loading';
import { GetLoggedInUserId } from '../../_services/authenticateService';
import moment from 'moment';
import constants from '../../_constants/constants';
import Constants from 'expo-constants';

const Messages = (props) => {
    const { navigation } = props;
    const { messageId, subject, threadId, userId, countNewMessage } = props.route.params;
    const [user, setUser] = useState({});
    const [messages, setMessages] = useState([]);
    const [textMessage, setTextMessage] = useState('');
    const [sendButtonColor, setSendButtonColor] = useState(Color.lightGreyBg);
    const [isChanged, setIsChanged] = useState(false);
    const [supportRequest, setSupportRequest] = useState({ UserId: '', EmailId: 'support@agmbenefits.com', Username: '', Subject: '', Message: '', ClientId:0,MessageId: 0, ThreadId: '', isPending: false });
    const [isLoading, setIsLoadingScreen] = useState(true);
    const [refreshing, setRefreshing] = useState(false);
    const [count, setCount] = useState(10);

    const scrollViewRef = useRef();
    navigation.setOptions({
        title: subject,
      });

    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        setCount(count+7);
        getConversation(threadId, count + 7);
    }, [count]);

    useEffect(() => {
        InitializeMessages();
    }, [isChanged]);

    const InitializeMessages = () => {
        getUser();
        getConversation(threadId, count).then(res => {
            ScrollToBottom();
        });
        countNewMessage > 0 ? postMessageIsRead() : '';
    }

    const ScrollToBottom = () => {
        setTimeout(() => {
            scrollViewRef.current.scrollToEnd({ animated: true });
        }, 200)
    };

    const getConversation = async (threadId, msgCount) => 
        await getConversationMessage(threadId, msgCount).then(res => {
            setIsChanged(false);
            setMessages(res);
            setIsLoadingScreen(false);
            setRefreshing(false);
        }).catch(err => {
            setIsLoadingScreen(false);
            console.log(err);
        })
    

    const getUser = async () => {
        await getToken().then(res => {
            setUser(res);
        }).catch(err => {
            console.log(err);
        });
    }
    const postMessageIsRead = async () => {
        supportRequest.ThreadId = threadId;
        supportRequest.UserId = userId;
        await postIsRead(supportRequest).then(res => {

        }).catch(err => { });
    }


    const sendSupportRequest = async () => {
        let newMessage = { Message: textMessage, UserId: user.UserID, EmailId: supportRequest.EmailId, Username: user.Username, ClientId: user.ClientID, Subject: subject, MessageId: messageId };
        setMessages([...messages, { ...newMessage, isPending: true, CreatedBy: await GetLoggedInUserId() }]);
        setIsChanged(true);
        await sendSupportMessage(newMessage).then(res => {
        }).catch(err => console.log(err))
    }

    const SetMessageVariable = (val) => {
        setTextMessage(val);
        setSendButtonColor((val !== '') ? Color.blueBg : Color.lightGreyBg);
    }

    return (

        <KeyboardAvoidingView
            behavior={Platform.OS === "ios" ? "padding" : "height"}
            keyboardVerticalOffset={65}
            style={[Color.lighterBg, Style.mainContainer]}
        >
            <ScrollView
                ref={scrollViewRef}
                // onContentSizeChange={() => ScrollToBottom()}
                style={[Color.lighterBg, { padding: 15 }]}
                contentContainerStyle={styles.scrollView}
                refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh} />}
            >
                {
                    isLoading ? <Loading /> :
                        _.isEmpty(messages) ?
                            <View style={{ width: '100%', alignItems: 'center', paddingTop: '15%' }}>
                                <Text allowFontScaling={false} style={{ fontSize: 18, fontFamily: 'OpenSans-SemiBold' }}>No Messages Available</Text>
                            </View> :
                            messages.map((message, i) =>

                                <View key={i} style={[Style.mainMessageBlockContainer, { justifyContent: message.CreatedBy === user.UserID ? "flex-end" : "flex-start" }]}>

                                    <View style={[Style.messageContainer, message.CreatedBy === user.UserID ? message.isPending ? Color.lightGreyBg : Color.greenBg : Color.lightGreyBg]}>
                                        <View style={[Style.messageBlockContainer]}>
                                            <Text allowFontScaling={false} style={[Style.messageText, message.CreatedBy === user.UserID ? Color.whiteColor : Color.blackColor]}>
                                                {message.Message}
                                            </Text>
                                        </View>

                                        <View style={[Style.timeStampContainer]}>
                                            <Text allowFontScaling={false} style={[Style.messageTimeStamp, message.CreatedBy === user.UserID ? Color.whiteColor : Color.blackColor]}>
                                                {moment(message.CreatedOn).format("ddd, MMM D, h:mm a")}
                                            </Text>
                                            <Animatable.View
                                                animation="zoomIn"
                                                duration={300}
                                                iterationCount={1}
                                            >
                                                <Icon
                                                    name="check-circle"
                                                    type="feather"
                                                    size={12}
                                                    iconStyle={[message.CreatedBy === user.UserID ? Color.whiteColor : Color.blackColor]}
                                                />
                                            </Animatable.View>
                                        </View>
                                    </View>
                                </View>
                            )
                }
            </ScrollView>

            <View style={[Style.messageMainInputContainer, Color.lighterBg]}>
                <View style={[Style.messageInputContainer]}>
                    <Input
                        value={textMessage}
                        allowFontScaling={false}
                        placeholder="New Message"
                        placeholderTextColor="#B1B8CA"
                        inputStyle={[Style.inputBorderedStyle]}
                        inputContainerStyle={[Style.inputBorderedContainer]}
                        containerStyle={[Style.inputBorderedMainContainer, {}]}
                        onFocus={() => ScrollToBottom()}
                        onChangeText={(val) => SetMessageVariable(val)}
                    />
                </View>
                <View style={[Style.messageSendButtonContainer]}>

                    <TouchableOpacity
                        disabled={textMessage == ''}
                        activeOpacity={0.5}
                        onPress={() => {
                            sendSupportRequest();
                            setTextMessage('');
                            setSendButtonColor(Color.lightGreyBg);
                            ScrollToBottom();
                        }}
                        style={[Style.messageSendIcon, sendButtonColor]}>
                        <Icon
                            name="send"
                            type="material-community"
                            size={25}
                            iconStyle={[Color.whiteColor, { margin: 0, paddingLeft: 3 }]}
                        />
                    </TouchableOpacity>
                </View>

            </View>
        </KeyboardAvoidingView>
    );
}

export default Messages;

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        // marginTop: Constants.statusBarHeight,
    },
    scrollView: {
        // flex: 1,
        // alignItems: 'center',
        // justifyContent: 'center',
    },
});