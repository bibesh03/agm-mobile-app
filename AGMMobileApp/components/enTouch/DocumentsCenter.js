import React, { useState, useEffect } from 'react';
import { ScrollView } from 'react-native-gesture-handler';
import { TouchableOpacity, View, Image, Modal, TouchableWithoutFeedback, Platform } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import * as DocumentPicker from 'expo-document-picker';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import Constants from 'expo-constants';
import { Icon, Input } from 'react-native-elements';
import { uploadDocuments } from '../../_services/documentsService';
import { getToken } from '../../_services/authenticateService';
import { Spinner, Text } from '@ui-kitten/components';
import Color from '../../assets/styles/colors';
import Style from '../../assets/styles/styles';
import { ShowToast } from '../../_actions/toast.action';
import { connect } from 'react-redux';
import DocumentScreen from '../NavStack/TabNavigation/DocumentScreen';
import MediaScreen from '../NavStack/TabNavigation/MediaScreen';
import Button from '../shared/Button';
import ActionBlock from '../shared/ActionBlock';

// const DocumentsCenter = (props) => {
const Tab = createBottomTabNavigator();

const DocumentsCenter = (props) => {
  const { ShowToast, navigation } = props;

  const [user, setUser] = useState({});
  const [clientId, setClientId] = useState(0);
  const [docAfterUpload, setDocAfterUpload] = useState(null);
  const [fileName, setFileName] = useState([]);
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [invalidFileName, setInvalidFileName] = useState(false);
  const [refresh, setRefresh] = useState(0);

  useEffect(() => {
    if (!modalVisible) {
      GetUser();
    }
  }, [modalVisible])

  const GetUser = async () => {
    await getToken().then(res => {
        setUser(res);
        setClientId(res.ClientID);
    }).catch(err => {
      console.log('error is', err);
    });
  }

  const save = (userID) => {
    let formData = new FormData();
    if (fileName[1] !== '') {
      ((fileName[1].includes(['pdf','doc','docx'])) ?
      (
        formData.append('files', {
          uri: docAfterUpload.uri,
          type: `application/${fileName[1]}`,
          name: `${fileName[0]}.${fileName[1]}`
        }))
        :
        formData.append('files', {
          uri: docAfterUpload.uri,
          type: `image/${fileName[1]}`,
          name: `${fileName[0]}.${fileName[1]}`
        })
        )

      uploadDocuments(userID, 0, 0, formData).then(res => {
        ShowToast({ 'message': "Successfully uploaded file.", 'type': 'success', "position": "top" });
        setIsSubmitting(false);
        setInvalidFileName(false);
        setDocAfterUpload(null);
        setFileName([]);
        setModalVisible(false);
        setRefresh(refresh + 1);
      }).catch(err => console.log(err)
      );
    }
    else {
      ShowToast({ 'message': "Couldn't upload the file", 'type': 'danger', "position": "top" });
      console.log('Couldnt upload file');
    }
  }

  const setFileNameFunctionality = (fileName) => {
    if (fileName !== '') {
      setFileName([fileName.split('.')[0], fileName.split('.')[fileName.split('.').length - 1].replace(/\s/g, '').toLowerCase()]);
    }
    else {
      setFileName(['', ''])
    }
  }

  const Doc = async () => {
    await DocumentPicker.getDocumentAsync({
      type: '*/*',
      copyToCacheDirectory: false,
    }).then(res => {
          if (res.type == 'success') {
            setDocAfterUpload(res);
            if (res.name.split('.').length !== 1) {
              setFileNameFunctionality(res.name);
            }
            else {
              setInvalidFileName(true);
              setFileNameFunctionality('');
              ShowToast({ 'message': "Couldn't upload the file. Invalid File extension", 'type': 'danger', "position": "top" });
              throw new Error('Invalid FileName');
            }
          };
          setModalVisible(true);
    }).catch(err => {
      console.log(err);
    });
  };

  const Camera = async () => {
    const result = await ImagePicker.launchCameraAsync({
      allowsEditing: true,
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      quality: 1,
    });
    if (!result.cancelled) {
      let nameArr = result.uri.split('/').pop().split('.');
      //console.log(nameArr);
      setFileName([nameArr[0], nameArr[1]]);
      setDocAfterUpload(result);
    }
  }

  const getPermissionAsync = async () => {
    if (Constants.platform.ios || Constants.platform.android) {
      const permission = await Permissions.askAsync(Permissions.CAMERA);
      if (permission.status !== 'granted') {
        return (<Text>Access to camera has been denied</Text>)
      }
      Camera();
    }
  };

  return (
    <>
      <TouchableOpacity
        activeOpacity={0.7}
        onPress={() => { setModalVisible(true) }}
        style={[Style.fixedButtonContainer]}
      >
        <Icon name="plus" type="feather" size={35} iconStyle={[Color.whiteColor, {}]} />
      </TouchableOpacity>

      <Tab.Navigator
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused }) => {
            if (route.name === 'DocumentView') {
              return (
                <Icon name='file-text' type='feather' size={20} color={focused ? Color.redColor.color : Color.midGreyColor.color} />
              );
            } else if (route.name === 'MediaView') {
              return (
                <Icon name='image' type='feather' size={20} color={focused ? Color.lightGreenColor.color : Color.midGreyColor.color} />
              );
            }
          }
        })}

        tabBarOptions={{
          allowFontScaling: false,
          // activeTintColor: '#42765E',
          activeTintColor: '#546062',
          inactiveTintColor: '#C0C3C4',
          labelPosition: 'below-icon',
          labelStyle: { fontSize: 12, fontWeight: '500' },
        }}
      >
        <Tab.Screen
          name='DocumentView'
          children={() => <DocumentScreen refresh={refresh} clientId={clientId} {...props} />}
          options={{
            tabBarLabel: 'Document',
          }}
        />
        <Tab.Screen
          name='MediaView' children={() => <MediaScreen refresh={refresh} clientId={clientId} {...props} />}
          options={{
            tabBarLabel: 'Media',
          }}
        />
      </Tab.Navigator>

      <Modal
        presentationStyle="pageSheet"
        animationType="slide"
        visible={modalVisible}
      >
        <TouchableWithoutFeedback
          onPressOut={(e) => {
            if (e.nativeEvent.locationY < 0) {
              setModalVisible(false)
            }
          }}
        >
          <View style={[Style.mainContainer, { paddingTop: '5%' }]}>
            <View style={[Style.titleBlockBorder, { paddingBottom: 10, }]}>
              <View style={[Style.containerPadding, { flexDirection: 'row', alignItems: 'center' }]}>
                <TouchableOpacity
                  activeOpacity={0.5}
                  style={{ width: '20%', alignContent: 'flex-start' }}
                  onPress={() => {
                    setDocAfterUpload(null);
                    setFileName([]);
                    setIsSubmitting(false);
                    setInvalidFileName(false);
                    setModalVisible(false);
                  }}
                >
                  <Text allowFontScaling={false} style={[Color.blueColor, { fontSize: 18, fontFamily: 'OpenSans-SemiBold' }]}>
                    Cancel
                      </Text>
                </TouchableOpacity>

                <View style={{ width: '60%', alignItems: 'center' }}>
                  <Text allowFontScaling={false} style={[Color.blackColor, { fontSize: 20, fontFamily: 'OpenSans-SemiBold' }]}>
                    Add Files
                    </Text>
                </View>
              </View>
            </View>

            <ScrollView style={[Style.containerPadding, Color.lighterBg, { paddingTop: Platform.OS === 'ios' ? '15%' : '10%' }]}>

              {
                docAfterUpload ?

                  <View style={[Style.imageContainer]}>
                    <View style={[Style.imageWrap]}>
                      {docAfterUpload ? (fileName[1] !== 'pdf' ? (<Image style={[Style.image]}
                        source={{ uri: docAfterUpload.uri }}
                      />) :
                        (
                          <>
                            <Image style={[Style.image]}
                              source={require('../../assets/images/pdfImg2.png')} />
                          </>)) : (<View />)}
                    </View>
                    {
                      docAfterUpload ?

                        <Input
                          allowFontScaling={false}
                          onChangeText={(e) => {
                            setFileName([e, fileName[1]]);
                          }}
                          defaultValue={docAfterUpload ? `${fileName[0]}` : ''}
                          rightIcon={<Icon name='edit-3' type='feather' iconStyle={[Color.blackColor, { opacity: 0.7 }]} />}
                          inputStyle={{ color: Color.blackColor.color, opacity: 0.8 }}
                          inputContainerStyle={{ borderWidth: 1, borderColor: "transparent", backgroundColor: "#ffffff", borderRadius: 5, paddingHorizontal: 15, paddingRight: 10, width: '100%', height: 56 }}
                          containerStyle={{ paddingHorizontal: 0 }}
                          clearButtonMode="while-editing"
                          keyboardType="default"
                        /> : <></>
                    }
                  </View>
                  :
                  <View style={[Style.imageContainer, { backgroundColor: Color.midGreyBg.backgroundColor, borderRadius: 10, opacity: 0.4 }]}>
                    <Icon name="add-to-photos" type="material" size={60} iconStyle={[Color.darkGreyColor, { opacity: 0.5 }]} />
                  </View>

              }

              <View style={{}}>
                {!docAfterUpload ?

                  (<>
                    <ActionBlock
                      title="Add File or Image"
                      iconRight="file-plus"
                      actionOnPress={() => {
                        setModalVisible(false)
                        setInvalidFileName(false);
                        setTimeout(() => Doc(), 850);
                      }}
                    />
                    <ActionBlock
                      title="Take Photo"
                      iconRight="camera"
                      actionOnPress={() => {
                        getPermissionAsync();
                      }}
                    />
                  </>) :
                  docAfterUpload.type !== 'image' ?
                    (
                      <>
                        <ActionBlock
                          title="Replace File"
                          iconRight="minus-circle"
                          actionOnPress={async () => {
                            setInvalidFileName(false);
                            setModalVisible(false);
                            setTimeout(() => Doc(), 850);
                          }}
                        />
                        <ActionBlock
                          title="Take Photo"
                          iconRight="camera"
                          actionOnPress={() => {
                            getPermissionAsync();
                          }}
                        />
                      </>
                    ) : (
                      <>
                        <ActionBlock
                          title="Add File or Image"
                          iconRight="file-plus"
                          actionOnPress={() => {
                            setInvalidFileName(false);
                            setModalVisible(false);
                            setTimeout(() => Doc(), 850);
                            setFileName([]);
                          }}
                        />
                        <ActionBlock
                          title="Replace Photo"
                          iconRight="upload"
                          actionOnPress={() => {
                            getPermissionAsync();
                            setFileName([])
                          }}
                        />
                      </>
                    )}
                <View style={{ marginTop: Platform.OS === 'ios' ? 40 : 100 }}>
                  <Button
                    buttonText="Save"
                    buttonColor={Color.blueBg}
                    activeOpacity={isSubmitting ? 1 : 0.5}
                    spinnerActive={isSubmitting}
                    actionOnPress={
                      isSubmitting || invalidFileName || fileName.length == 0 || fileName[0] == '' ?
                        () => { } :
                        () => {
                          setIsSubmitting(true);
                          save(user.UserID)
                        }
                    }
                  />
                </View>
              </View>
            </ScrollView>
          </View>
        </TouchableWithoutFeedback>

      </Modal>
    </>
  );
}
const mapDispatchToProps = {
  ShowToast
};

export default connect(null, mapDispatchToProps)(DocumentsCenter);