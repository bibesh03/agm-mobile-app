import React, { useState, useEffect } from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import CompletedTaskScreen from '../NavStack/TabNavigation/CompletedTaskScreen';
import IncompleteTaskScreen from '../NavStack/TabNavigation/IncompleteTaskScreen';
import { Icon } from 'react-native-elements';
import Color from '../../assets/styles/colors';
import _ from 'lodash';

const Tab = createBottomTabNavigator();

const Task = (props) => {

    return (
        <>
            <Tab.Navigator
                screenOptions={({ route }) => ({
                    tabBarIcon: ({ focused }) => {
                        if (route.name === 'IncompleteTasksView') {
                            return (
                                <Icon name='info' type='feather' size={20} color={focused ? Color.orangeColor.color : Color.midGreyColor.color} />
                            );
                        } else if (route.name === 'CompletedTaskView') {
                            return (
                                <Icon name='check-circle' type='feather' size={20} color={focused ? Color.lightGreenColor.color : Color.midGreyColor.color} />
                            );
                        }
                    }
                })}

                tabBarOptions={{
                    allowFontScaling: false,
                    activeTintColor: '#546062',
                    inactiveTintColor: '#C0C3C4',
                    labelPosition: 'below-icon',
                    labelStyle: { fontSize: 12, fontWeight: '500' },
                }}
            >
                <Tab.Screen
                    name='IncompleteTasksView'
                    children={() => <IncompleteTaskScreen {...props} />}
                    options={{
                        tabBarLabel: 'Incomplete',
                    }}
                />

                <Tab.Screen
                    name='CompletedTaskView'
                    children={() => <CompletedTaskScreen {...props} />}
                    options={{
                        tabBarLabel: 'Completed',
                    }}
                />
            </Tab.Navigator>
        </>
    );
}

export default Task;
