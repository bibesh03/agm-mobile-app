import React, { useEffect, useState, useRef } from 'react';
import { SafeAreaView, ScrollView, View, Text } from 'react-native';
import ListBlock from '../shared/ListBlock';
import Style from '../../assets/styles/styles';
import Color from '../../assets/styles/colors';
import Card from '../../assets/styles/card';
import Button from '../shared/Button';
import * as Animatable from 'react-native-animatable';
import { getToken } from '../../_services/authenticateService'
import moment from 'moment';
import { replace } from 'lodash';

const AccountInfo = (props) => {
    const { user } = props.route.params;

    const MaskSSN = (char, start, end, replacement) => char.substring(0, start) + replacement + char.substring(end);

    return (
        <SafeAreaView style={[Color.lighterBg, Style.mainContainer]}>
            <ScrollView style={[Style.containerPadding, { paddingTop: '7%' }]}>
                <Animatable.View
                    animation="fadeInUp"
                    duration={300}
                    delay={300}
                    iterationCount={1}
                    style={[Card.base, Color.whiteBg]}
                >
                    <View>
                        <ListBlock
                            title="Full Name"
                            subtitle={`${user.FirstName} ${user.LastName}`}
                            titleStyle={[Color.greenColor, Style.listBlockTitleAlt]}
                            subtitleStyle={[Style.listBlockSubtitleAlt]}
                            listBlockStyle={[Style.containerPadding]}
                        />

                        <ListBlock
                            title="Date of Birth"
                            subtitle={`${moment(user.DateOfBirth).format('MMMM DD, YYYY')}`}
                            titleStyle={[Color.greenColor, Style.listBlockTitleAlt]}
                            subtitleStyle={[Style.listBlockSubtitleAlt]}
                            listBlockStyle={[Style.containerPadding]}
                        />

                        <ListBlock
                            title="Email Address"
                            subtitle={`${user.Email}`}
                            titleStyle={[Color.greenColor, Style.listBlockTitleAlt]}
                            subtitleStyle={[Style.listBlockSubtitleAlt]}
                            listBlockStyle={[Style.containerPadding]}
                        />

                        <ListBlock
                            title="Username"
                            subtitle={`${user.Username}`}
                            titleStyle={[Color.greenColor, Style.listBlockTitleAlt]}
                            subtitleStyle={[Style.listBlockSubtitleAlt]}
                            listBlockStyle={[Style.containerPadding]}
                        />


                        <ListBlock
                            title="Social Security Number"
                            subtitle={MaskSSN(user.SSN, 0, 5, "••• •• ")}
                            titleStyle={[Color.greenColor, Style.listBlockTitleAlt]}
                            subtitleStyle={[Style.listBlockSubtitleAlt]}
                            listBlockStyle={[Style.containerPadding]}
                        />

                        <ListBlock
                            title="Group Registration ID"
                            subtitle={user.GroupRegistrationId}
                            titleStyle={[Color.greenColor, Style.listBlockTitleAlt]}
                            subtitleStyle={[Style.listBlockSubtitleAlt]}
                            listBlockStyle={[Style.containerPadding]}
                        />

                        <ListBlock
                            title="Hire Date"
                            subtitle={`${moment(user.HireDate).format('MMMM DD, YYYY')}`}
                            titleStyle={[Color.greenColor, Style.listBlockTitleAlt]}
                            subtitleStyle={[Style.listBlockSubtitleAlt]}
                            listBlockStyle={[Style.containerPadding, Style.listBlockFooter]}
                        />
                    </View>
                </Animatable.View>
            </ScrollView>
            <View style={[Style.containerPadding, { marginBottom: 20 }]}>
                <Button
                    buttonText="Update Account Info"
                    buttonColor={Color.blueBg}
                    actionOnPress={() => props.navigation.navigate('UpdateAccountInfo', { user: user })}
                />
            </View>
        </SafeAreaView >
    );
}

export default AccountInfo;