import React, { useState, useEffect } from 'react';
import { ScrollView, TouchableHighlight, TouchableOpacity } from 'react-native-gesture-handler';
import { SafeAreaView } from 'react-native';
import { View } from 'react-native';
import { Text } from '@ui-kitten/components';
import Style from '../../assets/styles/styles';
import Color from '../../assets/styles/colors';
import FontStyle from '../../assets/styles/font';
import CardMedium from '../shared/CardMedium';
import { getAllTasks } from '../../_services/taskService';
import { getNotification } from '../../_services/supportService';
import Loading from '../../_helpers/Loading';
import { GroupByIsChecked } from '../../_helpers/helper';
import _ from 'lodash';
import ListBlock from '../shared/ListBlock';

const NotificationCenter = (props) => {
    const { navigation } = props;
    const { task, count } = props.route.params;
    const [messageVisible, setMessageVisible] = useState(true);
    const [isLoadingTask, setIsLoadingScreenTask] = useState(true);
    const [isLoadingNotification, setIsLoadingScreenNotification] = useState(true);

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            setIsLoadingScreenNotification(false);
            setIsLoadingScreenTask(false);
        });
        return unsubscribe;
    }, [navigation]);

    return (
        <SafeAreaView style={[Color.lighterBg, Style.mainContainer]}>
            <ScrollView style={[Style.containerPadding, { paddingTop: '5%' }]}>

                <View style={{ marginBottom: count == 0 ? 0 : 40 }}>
                    {
                        isLoadingTask && isLoadingNotification ? <Loading /> :
                            _.isEmpty(task) && count == 0 ?

                                <View style={{ width: '100%', alignItems: 'center', paddingTop: '15%' }}>
                                    <Text allowFontScaling={false} style={{ fontSize: 18, fontFamily: 'OpenSans-SemiBold' }}>No Notifications Available</Text>
                                </View> :
                                count == 0 ? <></> :
                                    <>
                                        <View style={[Style.notyIndicator, Color.redBg]}>
                                            <Text allowFontScaling={false} style={[FontStyle.notySubtitle]}>
                                                New Messages ({count})
                                                </Text>
                                        </View>
                                    </>
                    }
                    {
                        _.times(count, (i) =>
                            <CardMedium
                                key={i}
                                header={'New Message'}
                                description={'You have a new message! Tap here to view more details.'}
                                icon='x'
                                titleStyle={[Color.greenColor, { fontSize: 16, fontFamily: 'OpenSans-SemiBold' }]}
                                descriptionStyle={[Color.blackColor, { fontSize: 14, fontFamily: 'OpenSans-Regular', opacity: 0.7 }]}
                                cardMediumStyle={{ minHeight: 60 }}
                                actionOnPress={() => navigation.navigate('MessageCenter')}

                            // actionIconOnPress={() => setMessageVisible(false)}
                            />
                        )
                    }
                </View>

                <View style={{ marginBottom: 40 }}>


                    {_.isEmpty(task) ? <></> :
                        <>
                            <View style={[Style.notyIndicator, Color.blueBg]}>
                                <Text allowFontScaling={false} style={[FontStyle.notySubtitle]}>
                                    New Tasks ({task.length})
                            </Text>
                            </View>
                        </>

                    }
                    {_.isEmpty(task) ? <></> :
                        task.map((data, i) =>
                            (!data.IsChecked ? <CardMedium
                                key={i}
                                header={'New Task'}
                                description={'You have a new task to complete! Tap here to view more details.'}
                                icon='chevron-right'
                                titleStyle={[Color.greenColor, { fontSize: 16, fontFamily: 'OpenSans-SemiBold' }]}
                                descriptionStyle={[Color.blackColor, { fontSize: 14, fontFamily: 'OpenSans-Regular', opacity: 0.7 }]}
                                iconSize={25}
                                iconStyle={{ marginLeft: 15, opacity: 0.7 }}
                                cardMediumStyle={{ minHeight: 60 }}
                                actionOnPress={() => navigation.navigate('IndividualTask', { clientEmployee_CETaskId: data.ClientEmployee_CETaskId })}
                                actionIconOnPress={() => setMessageVisible(false)}
                            /> : null)
                        )
                    }
                </View>

            </ScrollView>
        </SafeAreaView>
    )
}
export default NotificationCenter;
