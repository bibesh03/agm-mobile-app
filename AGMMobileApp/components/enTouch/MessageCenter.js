import React, { useState, useEffect } from 'react';
import { ScrollView } from 'react-native-gesture-handler';
import { SafeAreaView, View, TouchableOpacity, Alert, Platform } from 'react-native';
import { Text } from '@ui-kitten/components';
import { Icon } from 'react-native-elements';
import Color from '../../assets/styles/colors';
import Style from '../../assets/styles/styles';
import ListBlock from '../shared/ListBlock';
import { getToken } from '../../_services/authenticateService';
import { SwipeRow } from 'react-native-swipe-list-view';
import { getSupportMessage } from '../../_services/supportService';
import Loading from '../../_helpers/Loading';
import { SortByTimeDescending } from '../../_helpers/helper';
import _ from 'lodash';
import moment from 'moment';
import * as Animatable from 'react-native-animatable';
import { PlacementOptions } from '@ui-kitten/components/ui/popover/type';
// import {getIsNotReadCount} from '../../_services/supportService';

const MessageCenter = (props) => {
    const [user, setUser] = useState({});
    const [messages, setMessages] = useState([]);
    const [isLoading, setIsLoadingScreen] = useState(true);

    useEffect(() => {
        getUser();
        getSupportMessages();
    }, []);

    useEffect(() => {
        const unsubscribe = props.navigation.addListener('focus', () => {
            getUser();
            getSupportMessages();
            setIsLoadingScreen(true);
        });
        return unsubscribe;
    }, [props.navigation]);

    const getUser = async () => {
        await getToken().then(res => {
            setUser(res);

        }).catch(err => {
            console.log(err);
        })
        await getToken().then(res => {
            setUser(res);
        }).catch(err => console.log(err));
    }

    const getSupportMessages = async () => {
        await getSupportMessage().then(res => {
            setMessages(SortByTimeDescending(res));
            setIsLoadingScreen(false);
        }).catch(err => {
            setIsLoadingScreen(false);
            console.log(err);
        })
    }

    return (
        <SafeAreaView style={[Color.lighterBg, Style.mainContainer]}>
            {
                isLoading ? <></> :
                    Platform.OS === 'ios' ?
                        <Animatable.View
                            animation="fadeInUp"
                            duration={300}
                            delay={300}
                            iterationCount={1}
                            style={{ zIndex: 5 }}
                        >
                            <TouchableOpacity
                                activeOpacity={0.7}
                                onPress={() => props.navigation.navigate('NewMessageModal')}
                                style={[Style.fixedButtonContainer, Color.blueBg]}
                            >
                                <Icon name="plus" type="feather" size={35} iconStyle={[Color.whiteColor, {}]} />
                            </TouchableOpacity>
<<<<<<< HEAD
<<<<<<< HEAD
                        </Animatable.View> 
=======
                        </Animatable.View>
>>>>>>> 8e66378209e3ae9c1207318a6ee304b7d013dd78
=======
                        </Animatable.View>
=======
                        </Animatable.View> 
>>>>>>> v0.0.5
>>>>>>> 07f8b8e34d56c04b2f35ac59876df832b5735679
                        :
                        <TouchableOpacity
                            activeOpacity={0.7}
                            onPress={() => props.navigation.navigate('NewMessageModal')}
                            style={[Style.fixedButtonContainer, Color.blueBg]}
                        >
                            <Icon name="plus" type="feather" size={35} iconStyle={[Color.whiteColor, {}]} />
                        </TouchableOpacity>
            }
            <ScrollView style={{ paddingTop: '7%' }}>
                {
                    isLoading ? <Loading /> :
                        _.isEmpty(messages) ?
                            <Animatable.View
                                animation="fadeInUp"
                                duration={300}
                                delay={300}
                                iterationCount={1}
                                style={[Color.whiteBg, Style.containerPadding]}
                            >
                                <ListBlock
                                    title="No Messages Available"
                                />
                            </Animatable.View> :
                            messages.map((message, i) =>
                                <View key={i} style={[Color.whiteBg]}>
                                    <ListBlock
                                        key={i}
                                        title={"Subject: " + message.Subject}
                                        subtitle={moment(message.CreatedOn).format("ddd, MMM D, h:mm a")}
                                        badgeStatus={message.CountOfNewAdminMessages !== 0 ? message.CountOfNewAdminMessages : ""}
                                        badgeColor={message.CountOfNewAdminMessages !== 0 ? Color.redBg : Color.whiteBg}
                                        listBlockStyle={[Style.containerPadding]}
                                        iconRight="chevron-right"
                                        actionOnPress={() => props.navigation.navigate('Messages', { countNewMessage: message.CountOfNewAdminMessages, messageId: message.Id, subject: message.Subject, threadId: message.ThreadId, userId: user.UserID })}
                                    />
                                </View>
                            )
                }
            </ScrollView>
        </SafeAreaView >
    )
}
export default MessageCenter;
