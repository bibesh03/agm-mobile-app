import React, { useState, useEffect, useLayoutEffect } from 'react';
import { ScrollView } from 'react-native-gesture-handler';
import { SafeAreaView, View, TouchableOpacity, Modal, Linking, StatusBar } from 'react-native';
import Style from '../../assets/styles/styles';
import constants from '../../_constants/constants';
import Color from '../../assets/styles/colors';
import { Text } from '@ui-kitten/components';
import { Icon } from 'react-native-elements';
import CardBanner from '../shared/CardBanner';
import CardMedium from '../shared/CardMedium';
import CardSmall from '../shared/CardSmall';
import { enroll, getToken, storeToken } from '../../_services/authenticateService';
import _ from 'lodash';
import moment from 'moment';
import { getGroupUrl } from '../../_services/enrollService';
import { getAllTasks } from '../../_services/taskService';
import { getSupportMessage } from '../../_services/supportService';
import * as WebBrowser from 'expo-web-browser';
import Loading from '../../_helpers/LoadingSmallContainer'
import { GroupByIsChecked } from '../../_helpers/helper';
import { ShowToast } from '../../_actions/toast.action';
import { connect } from 'react-redux';

const Dashboard = (props) => {

  const { ShowToast, navigation } = props;

  const [notifications, setNotifications] = useState([]);
  const [messageVisible, setMessageVisible] = useState(true);
  const [messageVisibleEnrollment, setMessageVisibleEnrollment] = useState(true);
  const [result, setResult] = useState(null);
  const [task, setTask] = useState([]);
  const [groupUrl, setGroupUrl] = useState([]);
  const [user, setUser] = useState({});
  const [countTaskIncomplete, setCountTaskIncomplete] = useState(0);
  const [countMsg, setCountMsg] = useState(0);
  const [isLoading, setIsLoadingScreen] = useState(true);
  const [refresh, setRefresh] = useState(false);

  navigation.setOptions({
    title: 'Dashboard',
    headerLeft: ({ }) => (
      <>
        <Icon name='bell' type='feather' size={22} style={[Style.iconHeader]} onPress={() => navigation.navigate('NotificationCenter', { count: countMsg, task: GroupByIsChecked(task).false })}></Icon>
        {
          countMsg + countTaskIncomplete == 0 ? <></>
            :
            <View style={[Color.redBg, { position: 'absolute', left: Platform.OS === 'ios' ? 27 : 25, top: Platform.OS === 'ios' ? 5 : 11, paddingVertical: 2, paddingHorizontal: 5, borderRadius: 30, alignItems: 'center' }]}>
              <Text allowFontScaling={false} style={[Color.whiteColor, { fontSize: 8, fontFamily: 'OpenSans-SemiBold' }]}>{countMsg + countTaskIncomplete}</Text>
            </View>
        }
      </>),
    headerLeftContainerStyle: { padding: 15, paddingLeft: Platform.OS === 'ios' ? 10 : 15 },
    headerRight: () => (<Icon name='settings' type='feather' size={22} style={[Style.iconHeader]} onPress={() => navigation.navigate('Settings')}></Icon>),
    headerRightContainerStyle: { padding: 15, paddingRight: Platform.OS === 'ios' ? 10 : 15 },
  });
  useEffect(() => {
    getEnrollUrl();
    getTimeInterval(user.HireDate);
    getTasksToUpdateState();
    getNotifications();
    getUser();
    manageEnrollmentDate();
  }, []);

  useEffect(() => {
    getUser();

  }, [refresh]);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      getTasksToUpdateState();
      getTimeInterval(user.HireDate)
      getNotifications();
      manageEnrollmentDate();
      getUser();
      getEnrollUrl();
    });
    return unsubscribe;
  }, [navigation, refresh]);

  const _handleEnrollNowExpoBrowser = async () => {
    let result = await WebBrowser.openBrowserAsync(encodeURI(groupUrl)).then(res => {
    }).catch(err => {
      console.log(err);
    });
    setResult(result);
  };

  const _handleAGMInfoExpoBrowser = async () => {

    let result = await WebBrowser.openBrowserAsync('https://agmbenefitsolutions.com/').then(res => {
    }).catch(err => {
      console.log(err);
    });
  };

  const getEnrollUrl = async () => {
    await getGroupUrl().then(res => {
      setGroupUrl(res.Url);
    })
  }
  const enrollUser = async (isEnrolled) => {
    enroll(isEnrolled).then(res => {
      let updatedUser = { ...user };
      updatedUser.IsEnrolled = isEnrolled;
      storeToken(updatedUser);
      ShowToast({ 'message': res.data.Message, 'type': res.data.Success ? 'success' : 'warning', "position": "bottom" });
      setRefresh(true);
    }).catch(err => {
      console.log(err);
    })
  }
  const getUser = async () => {
    await getToken().then(res => {
      setUser(res);
    }).catch(err => console.log(err));
  }

  const manageEnrollmentDate = () => {
    var given = moment(user.HireDate, "YYYY-MM-DD");
    return moment(given).add(30, 'days').format('dddd, MMMM Do YYYY, h:mm a.');
  }

  const getTimeInterval = (hireDate) => {
    var given = moment(hireDate, "YYYY-MM-DD");
    var enrollmentDate = moment(given).add(30, 'days');
    var current = moment().startOf('day');
    //Difference in number of days
    var timeRemaining = Math.round(moment.duration(enrollmentDate.diff(current)).asDays());
    return twoDigit(timeRemaining);
  }

  function twoDigit(number) {
    var twodigit = number >= 10 ? number : "0" + number.toString();
    return twodigit;
  }

  const getTasksToUpdateState = async () => {
    await getAllTasks().then(res => {
      setIsLoadingScreen(false);
      setTask(res);
      SetCountOfIncompleteTasks(res);
    }).catch(err => {
      //error getting the task
    })
  }
  const SetCountOfIncompleteTasks = (res) => {
    let messageArray = GroupByIsChecked(res).false;
    _.isEmpty(messageArray) ? setCountTaskIncomplete(0) : setCountTaskIncomplete(messageArray.length);
  }

  const AddTotalNotificationsInEachThread = (arrayOfMsg) => {
    let countNewMessages = 0;
    arrayOfMsg.map(msg => {
      countNewMessages += msg.CountOfNewAdminMessages;
    });
    setCountMsg(countNewMessages);

  }

  const getNotifications = async () => {
    await getSupportMessage().then(res => {
      setNotifications(res);
      _.isEmpty(res) == false ?
        AddTotalNotificationsInEachThread(res) : setCountMsg(0);
    }).catch(err => {
    })
  }
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={[Color.lighterBg, Style.mainContainer]}>
        <ScrollView style={[Style.containerPadding, { paddingTop: '5%' }]}>
          {messageVisible ?
            <CardSmall
              header='Welcome to our new benefits mobile app!'
              description='Tap here to view more information.'
              icon='x'
              actionOnPress={() => _handleAGMInfoExpoBrowser()}
              actionIconOnPress={() => setMessageVisible(false)}
            /> : <></>
          }
          {user.IsEnrolled != true ? parseInt(getTimeInterval(user.HireDate)) > 0 ?
            <CardBanner
              number={`${getTimeInterval(user.HireDate)}`}
              subtitle={`Days remaining to enroll`}
              description={`Open Enrollment will end on ${manageEnrollmentDate()}`}
              buttonText={'Enroll Now'}
              actionOnPress={getTimeInterval(user.HireDate) > 0 ? () => {
                enrollUser(true);
                _handleEnrollNowExpoBrowser();

              } : () => { }}
            /> :
            !messageVisibleEnrollment ?
              <CardMedium
                header='Enrollment Period has Ended'
                description={`Open Enrollment ended on ${manageEnrollmentDate()}`}
                icon='x'
                titleStyle={{ fontSize: 30, fontWeight: '800', fontFamily: 'OpenSans-Bold', opacity: 0.8 }}
                actionIconOnPress={() => setMessageVisibleEnrollment(false)}
              /> : <></> : <></>
          }

          <CardMedium
            header='Benefits Center'
            description='Tap here to view important documents and other information about your benefits.'
            icon='arrow-right'
            actionOnPress={() => navigation.navigate('BenefitsCenter')}
          />
          <CardMedium
            header='My Documents'
            description='Tap to view or upload your photos and other important documents.'
            icon='folder'
            actionOnPress={() => navigation.navigate('DocumentsCenter')}
          />
          <CardMedium
            header='My Messages'
            description='Tap here to send and view your messages'
            icon='arrow-right'
            actionOnPress={() => navigation.navigate('MessageCenter')}
          />

          <View style={{ marginBottom: 40 }}>
            <View style={{ marginBottom: 15, flexDirection: 'row', justifyContent: 'space-between' }}>
              <View>
                <Text allowFontScaling={false} style={{ fontSize: 22, fontFamily: 'OpenSans-SemiBold' }}>My Tasks</Text>
              </View>
              {
                task.length !== 0 ?
                  <View>
                    <Icon name='arrow-right' type='feather' size={30} onPress={() => navigation.push('Task')} />
                  </View> : <></>
              }
            </View>

            {
              isLoading ? <Loading /> :
                task.length !== 0 ?

                  task.slice(0, 3).map(x =>

                    <TouchableOpacity key={x.ClientEmployee_CETaskId} onPress={() => navigation.push('IndividualTask', { clientEmployee_CETaskId: x.ClientEmployee_CETaskId })}>
                      <View style={[Style.taskBlock]}>
                        <View style={{ width: '10%' }}>
                          {x.IsChecked ? (<Icon name='check-circle' type='feather' color='#00AF54' size={25} />) : (<Icon name='info' type='feather' color='#FF7700' size={25} />)}
                        </View>

                        <View style={{ width: '80%' }}>
                          <Text allowFontScaling={false} style={[Style.taskTitle, { textTransform: 'capitalize' }]}>{x.Name}</Text>
                          {x.IsChecked ? (<Text allowFontScaling={false} style={[Style.taskStatus, Color.lightGreenColor]}>Complete</Text>) :
                            (<Text allowFontScaling={false} style={[Style.taskStatus, Color.orangeColor]}>Incomplete</Text>)}
                        </View>

                        <View style={{ width: '10%' }}>
                          <Icon name='chevron-right' type='feather' size={25} style={[Style.taskIconRight]} />
                        </View>
                      </View>
                    </TouchableOpacity>)
                  :
                  <Text allowFontScaling={false} style={{ fontWeight: '600' }}>No Tasks Available</Text>
            }
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  )
}

const mapDispatchToProps = {
  ShowToast
};
export default connect(null, mapDispatchToProps)(Dashboard);
