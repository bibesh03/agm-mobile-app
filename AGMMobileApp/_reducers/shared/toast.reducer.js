import ToastConstants from '../../_constants/toast.constants';
import { combineReducers } from 'redux';

const toastInitialState = {
    isVisible: false,
    message: "",
    type: "",
    position: "top"
}

const ToastReducer = (state = toastInitialState, action) => {
    switch(action.type) {
        case ToastConstants.SHOW_TOAST:
            return {
                isVisible: true,
                message: action.model.message,
                type: action.model.type,
                position: action.model.position
            };
        case ToastConstants.HIDE_TOAST:
            return toastInitialState;
        default:
            return state
    }
}

const ToastReducers = combineReducers({
    ToastReducer
})

export default ToastReducers