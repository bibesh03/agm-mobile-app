import { combineReducers } from 'redux';
import ToastReducers from './shared/toast.reducer';

export default combineReducers({
    ToastReducers
})